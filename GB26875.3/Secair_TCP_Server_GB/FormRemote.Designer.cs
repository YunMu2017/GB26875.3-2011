﻿namespace Secair_TCP_Server_GB
{
    partial class FormRemote
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormRemote));
            this.panel1 = new System.Windows.Forms.Panel();
            this.chkSecairLog = new System.Windows.Forms.CheckBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.chkDebugPlatform = new System.Windows.Forms.CheckBox();
            this.btnTcpPlatformStop = new System.Windows.Forms.Button();
            this.txtTcpPlatformPort = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtTcpPlatformIp = new System.Windows.Forms.TextBox();
            this.btnTcpPlatformStart = new System.Windows.Forms.Button();
            this.pic_SP = new System.Windows.Forms.PictureBox();
            this.btnStopTcp = new System.Windows.Forms.Button();
            this.btnStartTcp = new System.Windows.Forms.Button();
            this.txtHttpPort = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.chkDebug = new System.Windows.Forms.CheckBox();
            this.chkLite = new System.Windows.Forms.CheckBox();
            this.btnStopHttp = new System.Windows.Forms.Button();
            this.btnStartHttp = new System.Windows.Forms.Button();
            this.txtTcpPort = new System.Windows.Forms.TextBox();
            this.pnlMiddle = new System.Windows.Forms.Panel();
            this.tabControlMain = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dataGridView_GateWay = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.lbxMsg = new System.Windows.Forms.ListBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.txtLog = new System.Windows.Forms.TextBox();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.txtLogSecair = new System.Windows.Forms.TextBox();
            this.pic_Main_Min = new System.Windows.Forms.PictureBox();
            this.pic_Main_Close = new System.Windows.Forms.PictureBox();
            this.lbl_ClearEqu = new System.Windows.Forms.Label();
            this.lbl_ClearLocal = new System.Windows.Forms.Label();
            this.lbl_ClearPlatform = new System.Windows.Forms.Label();
            this.lbl_ClearSecair = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_SP)).BeginInit();
            this.pnlMiddle.SuspendLayout();
            this.tabControlMain.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_GateWay)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Main_Min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Main_Close)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.chkSecairLog);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.chkDebugPlatform);
            this.panel1.Controls.Add(this.btnTcpPlatformStop);
            this.panel1.Controls.Add(this.txtTcpPlatformPort);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.txtTcpPlatformIp);
            this.panel1.Controls.Add(this.btnTcpPlatformStart);
            this.panel1.Controls.Add(this.pic_SP);
            this.panel1.Controls.Add(this.btnStopTcp);
            this.panel1.Controls.Add(this.btnStartTcp);
            this.panel1.Controls.Add(this.txtHttpPort);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.chkDebug);
            this.panel1.Controls.Add(this.chkLite);
            this.panel1.Controls.Add(this.btnStopHttp);
            this.panel1.Controls.Add(this.btnStartHttp);
            this.panel1.Controls.Add(this.txtTcpPort);
            this.panel1.Location = new System.Drawing.Point(1, 40);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(978, 80);
            this.panel1.TabIndex = 4;
            // 
            // chkSecairLog
            // 
            this.chkSecairLog.AutoSize = true;
            this.chkSecairLog.Location = new System.Drawing.Point(100, 53);
            this.chkSecairLog.Name = "chkSecairLog";
            this.chkSecairLog.Size = new System.Drawing.Size(78, 16);
            this.chkSecairLog.TabIndex = 40;
            this.chkSecairLog.Text = "Debug/Log";
            this.chkSecairLog.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label8.Location = new System.Drawing.Point(4, 55);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(47, 12);
            this.label8.TabIndex = 38;
            this.label8.Text = "Secair";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label7.Location = new System.Drawing.Point(4, 15);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(40, 12);
            this.label7.TabIndex = 37;
            this.label7.Text = "Loacl";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.DarkGray;
            this.pictureBox1.Location = new System.Drawing.Point(0, 40);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(580, 1);
            this.pictureBox1.TabIndex = 36;
            this.pictureBox1.TabStop = false;
            // 
            // chkDebugPlatform
            // 
            this.chkDebugPlatform.AutoSize = true;
            this.chkDebugPlatform.Location = new System.Drawing.Point(590, 55);
            this.chkDebugPlatform.Name = "chkDebugPlatform";
            this.chkDebugPlatform.Size = new System.Drawing.Size(78, 16);
            this.chkDebugPlatform.TabIndex = 35;
            this.chkDebugPlatform.Text = "Debug/Log";
            this.chkDebugPlatform.UseVisualStyleBackColor = true;
            // 
            // btnTcpPlatformStop
            // 
            this.btnTcpPlatformStop.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTcpPlatformStop.Enabled = false;
            this.btnTcpPlatformStop.Location = new System.Drawing.Point(888, 47);
            this.btnTcpPlatformStop.Name = "btnTcpPlatformStop";
            this.btnTcpPlatformStop.Size = new System.Drawing.Size(68, 26);
            this.btnTcpPlatformStop.TabIndex = 34;
            this.btnTcpPlatformStop.Text = "StopTcp";
            this.btnTcpPlatformStop.UseVisualStyleBackColor = true;
            this.btnTcpPlatformStop.Click += new System.EventHandler(this.btnTcpPlatformStop_Click);
            // 
            // txtTcpPlatformPort
            // 
            this.txtTcpPlatformPort.Location = new System.Drawing.Point(895, 13);
            this.txtTcpPlatformPort.Name = "txtTcpPlatformPort";
            this.txtTcpPlatformPort.Size = new System.Drawing.Size(40, 21);
            this.txtTcpPlatformPort.TabIndex = 33;
            this.txtTcpPlatformPort.Text = "2000";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label5.Location = new System.Drawing.Point(861, 15);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(33, 12);
            this.label5.TabIndex = 32;
            this.label5.Text = "PORT";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label4.Location = new System.Drawing.Point(731, 15);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(19, 12);
            this.label4.TabIndex = 31;
            this.label4.Text = "IP";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.Location = new System.Drawing.Point(590, 15);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(110, 12);
            this.label3.TabIndex = 30;
            this.label3.Text = "tcp(ToPlatform)";
            // 
            // txtTcpPlatformIp
            // 
            this.txtTcpPlatformIp.Location = new System.Drawing.Point(750, 13);
            this.txtTcpPlatformIp.Name = "txtTcpPlatformIp";
            this.txtTcpPlatformIp.Size = new System.Drawing.Size(100, 21);
            this.txtTcpPlatformIp.TabIndex = 29;
            this.txtTcpPlatformIp.Text = "58.211.247.6";
            // 
            // btnTcpPlatformStart
            // 
            this.btnTcpPlatformStart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTcpPlatformStart.Location = new System.Drawing.Point(798, 47);
            this.btnTcpPlatformStart.Name = "btnTcpPlatformStart";
            this.btnTcpPlatformStart.Size = new System.Drawing.Size(68, 26);
            this.btnTcpPlatformStart.TabIndex = 28;
            this.btnTcpPlatformStart.Text = "StartTcp";
            this.btnTcpPlatformStart.UseVisualStyleBackColor = true;
            this.btnTcpPlatformStart.Click += new System.EventHandler(this.btnTcpPlatformStart_Click);
            // 
            // pic_SP
            // 
            this.pic_SP.BackColor = System.Drawing.Color.DarkGray;
            this.pic_SP.Location = new System.Drawing.Point(580, 0);
            this.pic_SP.Name = "pic_SP";
            this.pic_SP.Size = new System.Drawing.Size(1, 80);
            this.pic_SP.TabIndex = 27;
            this.pic_SP.TabStop = false;
            // 
            // btnStopTcp
            // 
            this.btnStopTcp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStopTcp.Location = new System.Drawing.Point(498, 7);
            this.btnStopTcp.Name = "btnStopTcp";
            this.btnStopTcp.Size = new System.Drawing.Size(68, 26);
            this.btnStopTcp.TabIndex = 26;
            this.btnStopTcp.Text = "StopTcp";
            this.btnStopTcp.UseVisualStyleBackColor = true;
            this.btnStopTcp.Click += new System.EventHandler(this.btnStopTcp_Click);
            // 
            // btnStartTcp
            // 
            this.btnStartTcp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStartTcp.Location = new System.Drawing.Point(423, 7);
            this.btnStartTcp.Name = "btnStartTcp";
            this.btnStartTcp.Size = new System.Drawing.Size(68, 26);
            this.btnStartTcp.TabIndex = 25;
            this.btnStartTcp.Text = "StartTcp";
            this.btnStartTcp.UseVisualStyleBackColor = true;
            this.btnStartTcp.Click += new System.EventHandler(this.btnStartTcp_Click);
            // 
            // txtHttpPort
            // 
            this.txtHttpPort.Location = new System.Drawing.Point(210, 13);
            this.txtHttpPort.Name = "txtHttpPort";
            this.txtHttpPort.Size = new System.Drawing.Size(40, 21);
            this.txtHttpPort.TabIndex = 24;
            this.txtHttpPort.Text = "24444";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(180, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 12);
            this.label2.TabIndex = 23;
            this.label2.Text = "http";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(100, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(23, 12);
            this.label1.TabIndex = 22;
            this.label1.Text = "tcp";
            // 
            // chkDebug
            // 
            this.chkDebug.AutoSize = true;
            this.chkDebug.Location = new System.Drawing.Point(335, 15);
            this.chkDebug.Name = "chkDebug";
            this.chkDebug.Size = new System.Drawing.Size(78, 16);
            this.chkDebug.TabIndex = 21;
            this.chkDebug.Text = "Debug/Log";
            this.chkDebug.UseVisualStyleBackColor = true;
            // 
            // chkLite
            // 
            this.chkLite.AutoSize = true;
            this.chkLite.Checked = true;
            this.chkLite.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkLite.Location = new System.Drawing.Point(280, 15);
            this.chkLite.Name = "chkLite";
            this.chkLite.Size = new System.Drawing.Size(48, 16);
            this.chkLite.TabIndex = 20;
            this.chkLite.Text = "Lite";
            this.chkLite.UseVisualStyleBackColor = true;
            // 
            // btnStopHttp
            // 
            this.btnStopHttp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStopHttp.Location = new System.Drawing.Point(498, 47);
            this.btnStopHttp.Name = "btnStopHttp";
            this.btnStopHttp.Size = new System.Drawing.Size(68, 26);
            this.btnStopHttp.TabIndex = 18;
            this.btnStopHttp.Text = "StopHttp";
            this.btnStopHttp.UseVisualStyleBackColor = true;
            this.btnStopHttp.Click += new System.EventHandler(this.btnStopHttp_Click);
            // 
            // btnStartHttp
            // 
            this.btnStartHttp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStartHttp.Location = new System.Drawing.Point(423, 47);
            this.btnStartHttp.Name = "btnStartHttp";
            this.btnStartHttp.Size = new System.Drawing.Size(68, 26);
            this.btnStartHttp.TabIndex = 17;
            this.btnStartHttp.Text = "StartHttp";
            this.btnStartHttp.UseVisualStyleBackColor = true;
            this.btnStartHttp.Click += new System.EventHandler(this.btnStartHttp_Click);
            // 
            // txtTcpPort
            // 
            this.txtTcpPort.Location = new System.Drawing.Point(125, 13);
            this.txtTcpPort.Name = "txtTcpPort";
            this.txtTcpPort.Size = new System.Drawing.Size(40, 21);
            this.txtTcpPort.TabIndex = 16;
            this.txtTcpPort.Text = "14444";
            // 
            // pnlMiddle
            // 
            this.pnlMiddle.BackColor = System.Drawing.Color.White;
            this.pnlMiddle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlMiddle.Controls.Add(this.tabControlMain);
            this.pnlMiddle.Location = new System.Drawing.Point(1, 120);
            this.pnlMiddle.Name = "pnlMiddle";
            this.pnlMiddle.Size = new System.Drawing.Size(978, 410);
            this.pnlMiddle.TabIndex = 5;
            // 
            // tabControlMain
            // 
            this.tabControlMain.Controls.Add(this.tabPage1);
            this.tabControlMain.Controls.Add(this.tabPage2);
            this.tabControlMain.Controls.Add(this.tabPage3);
            this.tabControlMain.Controls.Add(this.tabPage4);
            this.tabControlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlMain.Location = new System.Drawing.Point(0, 0);
            this.tabControlMain.Name = "tabControlMain";
            this.tabControlMain.SelectedIndex = 0;
            this.tabControlMain.Size = new System.Drawing.Size(976, 408);
            this.tabControlMain.TabIndex = 9;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dataGridView_GateWay);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(968, 382);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "联网设备列表";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // dataGridView_GateWay
            // 
            this.dataGridView_GateWay.AllowUserToAddRows = false;
            this.dataGridView_GateWay.AllowUserToOrderColumns = true;
            this.dataGridView_GateWay.BackgroundColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView_GateWay.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView_GateWay.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_GateWay.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5,
            this.Column6,
            this.Column7,
            this.Column9});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView_GateWay.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView_GateWay.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView_GateWay.GridColor = System.Drawing.Color.Gainsboro;
            this.dataGridView_GateWay.Location = new System.Drawing.Point(3, 3);
            this.dataGridView_GateWay.Name = "dataGridView_GateWay";
            this.dataGridView_GateWay.RowTemplate.Height = 23;
            this.dataGridView_GateWay.Size = new System.Drawing.Size(962, 376);
            this.dataGridView_GateWay.TabIndex = 4;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "编号";
            this.Column1.Name = "Column1";
            // 
            // Column2
            // 
            this.Column2.HeaderText = "厂家";
            this.Column2.Name = "Column2";
            // 
            // Column3
            // 
            this.Column3.HeaderText = "装置地址";
            this.Column3.Name = "Column3";
            // 
            // Column4
            // 
            this.Column4.HeaderText = "协议时间";
            this.Column4.Name = "Column4";
            this.Column4.Width = 150;
            // 
            // Column5
            // 
            this.Column5.HeaderText = "本地时间";
            this.Column5.Name = "Column5";
            this.Column5.Width = 150;
            // 
            // Column6
            // 
            this.Column6.HeaderText = "接收间隔";
            this.Column6.Name = "Column6";
            // 
            // Column7
            // 
            this.Column7.HeaderText = "最近上传协议";
            this.Column7.Name = "Column7";
            // 
            // Column9
            // 
            this.Column9.HeaderText = "备注";
            this.Column9.Name = "Column9";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.lbxMsg);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(968, 382);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "本地服务日志";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // lbxMsg
            // 
            this.lbxMsg.BackColor = System.Drawing.Color.White;
            this.lbxMsg.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lbxMsg.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbxMsg.FormattingEnabled = true;
            this.lbxMsg.HorizontalScrollbar = true;
            this.lbxMsg.ItemHeight = 12;
            this.lbxMsg.Location = new System.Drawing.Point(3, 3);
            this.lbxMsg.Name = "lbxMsg";
            this.lbxMsg.Size = new System.Drawing.Size(962, 376);
            this.lbxMsg.TabIndex = 20;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.txtLog);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(968, 382);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "总队平台交互日志";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // txtLog
            // 
            this.txtLog.BackColor = System.Drawing.SystemColors.Window;
            this.txtLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtLog.Location = new System.Drawing.Point(0, 0);
            this.txtLog.Margin = new System.Windows.Forms.Padding(2);
            this.txtLog.Multiline = true;
            this.txtLog.Name = "txtLog";
            this.txtLog.ReadOnly = true;
            this.txtLog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtLog.Size = new System.Drawing.Size(968, 382);
            this.txtLog.TabIndex = 7;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.txtLogSecair);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(968, 382);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "平台交互日志";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // txtLogSecair
            // 
            this.txtLogSecair.BackColor = System.Drawing.SystemColors.Window;
            this.txtLogSecair.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtLogSecair.Location = new System.Drawing.Point(0, 0);
            this.txtLogSecair.Margin = new System.Windows.Forms.Padding(2);
            this.txtLogSecair.Multiline = true;
            this.txtLogSecair.Name = "txtLogSecair";
            this.txtLogSecair.ReadOnly = true;
            this.txtLogSecair.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtLogSecair.Size = new System.Drawing.Size(968, 382);
            this.txtLogSecair.TabIndex = 8;
            // 
            // pic_Main_Min
            // 
            this.pic_Main_Min.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pic_Main_Min.BackColor = System.Drawing.Color.Transparent;
            this.pic_Main_Min.BackgroundImage = global::Secair_TCP_Server_GB.Properties.Resources.form_MinN;
            this.pic_Main_Min.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pic_Main_Min.Location = new System.Drawing.Point(920, 0);
            this.pic_Main_Min.Name = "pic_Main_Min";
            this.pic_Main_Min.Size = new System.Drawing.Size(24, 24);
            this.pic_Main_Min.TabIndex = 48;
            this.pic_Main_Min.TabStop = false;
            this.pic_Main_Min.Click += new System.EventHandler(this.pic_Main_Min_Click);
            this.pic_Main_Min.MouseEnter += new System.EventHandler(this.pic_Main_Min_MouseEnter);
            this.pic_Main_Min.MouseLeave += new System.EventHandler(this.pic_Main_Min_MouseLeave);
            // 
            // pic_Main_Close
            // 
            this.pic_Main_Close.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pic_Main_Close.BackColor = System.Drawing.Color.Transparent;
            this.pic_Main_Close.BackgroundImage = global::Secair_TCP_Server_GB.Properties.Resources.form_CloseN;
            this.pic_Main_Close.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pic_Main_Close.Location = new System.Drawing.Point(950, 0);
            this.pic_Main_Close.Name = "pic_Main_Close";
            this.pic_Main_Close.Size = new System.Drawing.Size(24, 24);
            this.pic_Main_Close.TabIndex = 47;
            this.pic_Main_Close.TabStop = false;
            this.pic_Main_Close.Click += new System.EventHandler(this.pic_Main_Close_Click);
            this.pic_Main_Close.MouseEnter += new System.EventHandler(this.pic_Main_Close_MouseEnter);
            this.pic_Main_Close.MouseLeave += new System.EventHandler(this.pic_Main_Close_MouseLeave);
            // 
            // lbl_ClearEqu
            // 
            this.lbl_ClearEqu.AutoSize = true;
            this.lbl_ClearEqu.BackColor = System.Drawing.Color.Transparent;
            this.lbl_ClearEqu.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_ClearEqu.ForeColor = System.Drawing.Color.White;
            this.lbl_ClearEqu.Location = new System.Drawing.Point(20, 537);
            this.lbl_ClearEqu.Name = "lbl_ClearEqu";
            this.lbl_ClearEqu.Size = new System.Drawing.Size(101, 12);
            this.lbl_ClearEqu.TabIndex = 233;
            this.lbl_ClearEqu.Text = "清除联网设备列表";
            this.lbl_ClearEqu.Click += new System.EventHandler(this.lbl_ClearEqu_Click);
            this.lbl_ClearEqu.MouseEnter += new System.EventHandler(this.lblValue_MouseEnter);
            this.lbl_ClearEqu.MouseLeave += new System.EventHandler(this.lblValue_MouseLeave);
            // 
            // lbl_ClearLocal
            // 
            this.lbl_ClearLocal.AutoSize = true;
            this.lbl_ClearLocal.BackColor = System.Drawing.Color.Transparent;
            this.lbl_ClearLocal.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_ClearLocal.ForeColor = System.Drawing.Color.White;
            this.lbl_ClearLocal.Location = new System.Drawing.Point(170, 537);
            this.lbl_ClearLocal.Name = "lbl_ClearLocal";
            this.lbl_ClearLocal.Size = new System.Drawing.Size(101, 12);
            this.lbl_ClearLocal.TabIndex = 234;
            this.lbl_ClearLocal.Text = "清除本地服务日志";
            this.lbl_ClearLocal.Click += new System.EventHandler(this.lbl_ClearLocal_Click);
            this.lbl_ClearLocal.MouseEnter += new System.EventHandler(this.lblValue_MouseEnter);
            this.lbl_ClearLocal.MouseLeave += new System.EventHandler(this.lblValue_MouseLeave);
            // 
            // lbl_ClearPlatform
            // 
            this.lbl_ClearPlatform.AutoSize = true;
            this.lbl_ClearPlatform.BackColor = System.Drawing.Color.Transparent;
            this.lbl_ClearPlatform.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_ClearPlatform.ForeColor = System.Drawing.Color.White;
            this.lbl_ClearPlatform.Location = new System.Drawing.Point(320, 537);
            this.lbl_ClearPlatform.Name = "lbl_ClearPlatform";
            this.lbl_ClearPlatform.Size = new System.Drawing.Size(101, 12);
            this.lbl_ClearPlatform.TabIndex = 235;
            this.lbl_ClearPlatform.Text = "清除总队平台日志";
            this.lbl_ClearPlatform.Click += new System.EventHandler(this.lbl_ClearPlatform_Click);
            this.lbl_ClearPlatform.MouseEnter += new System.EventHandler(this.lblValue_MouseEnter);
            this.lbl_ClearPlatform.MouseLeave += new System.EventHandler(this.lblValue_MouseLeave);
            // 
            // lbl_ClearSecair
            // 
            this.lbl_ClearSecair.AutoSize = true;
            this.lbl_ClearSecair.BackColor = System.Drawing.Color.Transparent;
            this.lbl_ClearSecair.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_ClearSecair.ForeColor = System.Drawing.Color.White;
            this.lbl_ClearSecair.Location = new System.Drawing.Point(470, 537);
            this.lbl_ClearSecair.Name = "lbl_ClearSecair";
            this.lbl_ClearSecair.Size = new System.Drawing.Size(77, 12);
            this.lbl_ClearSecair.TabIndex = 236;
            this.lbl_ClearSecair.Text = "清除平台日志";
            this.lbl_ClearSecair.Click += new System.EventHandler(this.lbl_ClearSecair_Click);
            this.lbl_ClearSecair.MouseEnter += new System.EventHandler(this.lblValue_MouseEnter);
            this.lbl_ClearSecair.MouseLeave += new System.EventHandler(this.lblValue_MouseLeave);
            // 
            // FormRemote
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(980, 558);
            this.Controls.Add(this.lbl_ClearSecair);
            this.Controls.Add(this.lbl_ClearPlatform);
            this.Controls.Add(this.lbl_ClearLocal);
            this.Controls.Add(this.lbl_ClearEqu);
            this.Controls.Add(this.pic_Main_Min);
            this.Controls.Add(this.pic_Main_Close);
            this.Controls.Add(this.pnlMiddle);
            this.Controls.Add(this.panel1);
            this.ForeColor = System.Drawing.Color.Black;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FormRemote";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "国标控制台";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormRemote_FormClosing);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_SP)).EndInit();
            this.pnlMiddle.ResumeLayout(false);
            this.tabControlMain.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_GateWay)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Main_Min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Main_Close)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox chkDebug;
        private System.Windows.Forms.CheckBox chkLite;
        private System.Windows.Forms.Button btnStopHttp;
        private System.Windows.Forms.Button btnStartHttp;
        private System.Windows.Forms.TextBox txtTcpPort;
        private System.Windows.Forms.TextBox txtHttpPort;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnStopTcp;
        private System.Windows.Forms.Button btnStartTcp;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtTcpPlatformIp;
        private System.Windows.Forms.Button btnTcpPlatformStart;
        private System.Windows.Forms.PictureBox pic_SP;
        private System.Windows.Forms.Button btnTcpPlatformStop;
        private System.Windows.Forms.TextBox txtTcpPlatformPort;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox chkDebugPlatform;
        private System.Windows.Forms.Panel pnlMiddle;
        private System.Windows.Forms.PictureBox pic_Main_Min;
        private System.Windows.Forms.PictureBox pic_Main_Close;
        private System.Windows.Forms.TabControl tabControlMain;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.ListBox lbxMsg;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TextBox txtLog;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.DataGridView dataGridView_GateWay;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.Label lbl_ClearEqu;
        private System.Windows.Forms.Label lbl_ClearLocal;
        private System.Windows.Forms.Label lbl_ClearPlatform;
        private System.Windows.Forms.Label lbl_ClearSecair;
        private System.Windows.Forms.TextBox txtLogSecair;
        private System.Windows.Forms.CheckBox chkSecairLog;
    }
}

