﻿using System;

namespace Secair_TCP_Server_GB
{
    public class TcpClientInfo
    {
        public IntPtr connectId;
        public string ip;
        public ushort port;
        public string deviceId;
    }
}
