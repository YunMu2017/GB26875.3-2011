﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Secair.Core
{
    public enum CommandEnum
    {
        [Description("预留")]
        Command0 = 0,
        [Description("控制命令")]
        Command1,
        [Description("发送数据")]
        Command2,
        [Description("确认")]
        Command3,
        [Description("请求")]
        Command4,
        [Description("应答")]
        Command5,
        [Description("否认")]
        Command6,
    }
}
