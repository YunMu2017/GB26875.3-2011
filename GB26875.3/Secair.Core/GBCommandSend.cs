﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Secair.Core;
using Secair.Core.Tool;

namespace Secair.Core
{

    /// <summary>
    /// 国标命令和应用数据单元类型响应
    /// </summary>
    public static class GBCommandSend
    {
        /// <summary>
        /// 命令2，下发数据
        /// </summary>
        public static void send2()
        { 
        
        }

        /// <summary>
        /// 命令3，确认，下行
        /// </summary>
        /// <param name="gbMessageEntity">设备上报的数据转的实体</param>
        /// <param name="upPayLoad">设备上报的字符串</param>
        /// <returns></returns>
        public static byte[] send3(GBMessageEntity gbMessageEntity, string upPayLoad)
        {
            //组装要下发的数据
            byte[] temp = ByteHelper.hexStringToBytes(upPayLoad);
            temp[26] = (byte)Convert.ToInt32("03", 16);
            //前28字节复制
            byte[] result = new byte[30];
            Buffer.BlockCopy(temp, 0, result, 0, 27);
            //时间重置
            result[6] = (byte)DateTime.Now.Second;
            result[7] = (byte)DateTime.Now.Minute;
            result[8] = (byte)DateTime.Now.Hour;
            result[9] = (byte)DateTime.Now.Day;
            result[10] = (byte)DateTime.Now.Month;
            result[11] = (byte)(DateTime.Now.Year % 2000);
            //交换地址
            Buffer.BlockCopy(temp, 12, result, 18, 6);
            Buffer.BlockCopy(temp, 18, result, 12, 6);
            //长度置0
            result[24] = 0;
            result[25] = 0;
            //校验位
            byte[] tempCheck = new byte[25];
            Buffer.BlockCopy(result, 2, tempCheck, 0, 25);
            result[27] = GBCheck.CS(tempCheck);
            //##
            result[28] = (byte)Convert.ToInt32("23", 16);
            result[29] = (byte)Convert.ToInt32("23", 16);

            return result;
        }

        /// <summary>
        /// 命令6，否认
        /// </summary>
        public static byte[] send6(GBMessageEntity gbMessageEntity, string upPayLoad)
        {
            byte[] temp = ByteHelper.hexStringToBytes(upPayLoad);
            temp[26] = (byte)Convert.ToInt32("06", 16);
            byte[] tempCheck = new byte[25];
            Buffer.BlockCopy(temp, 2, tempCheck, 0, 25);
            temp[27] = GBCheck.CS(tempCheck);

            //前28字节复制
            byte[] result = new byte[30];
            Buffer.BlockCopy(temp, 0, result, 0, 28);
            //时间重置
            result[6] = (byte)DateTime.Now.Second;
            result[7] = (byte)DateTime.Now.Minute;
            result[8] = (byte)DateTime.Now.Hour;
            result[9] = (byte)DateTime.Now.Day;
            result[10] = (byte)DateTime.Now.Month;
            result[11] = (byte)(DateTime.Now.Year % 2000);
            //交换地址
            Buffer.BlockCopy(temp, 12, result, 18, 6);
            Buffer.BlockCopy(temp, 18, result, 12, 6);
            //长度置0
            result[24] = 0;
            result[25] = 0;
            //##
            result[28] = (byte)Convert.ToInt32("23", 16);
            result[29] = (byte)Convert.ToInt32("23", 16);

            return result;
        }

        /// <summary>
        /// 命令1，控制命令
        /// </summary>
        public static void send1()
        {

        }

        /// <summary>
        /// 命令4，请求，下行查询请求
        /// </summary>
        public static void send4()
        {
            //查岗
        }
    }
}
