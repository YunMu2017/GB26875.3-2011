﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using Secair.Core.Data;
using Secair.Core.Tool;

namespace Secair.Core
{
    public static class GBCommandHandler
    {
        /// <summary>
        /// 处理命令1，控制命令
        /// </summary>
        /// <param name="commandType"></param>
        /// <param name="messageEntity"></param>
        /// <returns>返回 处理结果</returns>
        public static Data1 handler1(int commandType, GBMessageEntity messageEntity)
        {
            //string result = "";
            Data1 result = new Data1();
            switch (commandType)
            {
                case 90:
                    result.type = 3;
                    result.msg = "同步用户信息传输装置时间";
                    result.msgLog = "同步用户信息传输装置时间";

                    break;
                case 91:
                    result.msgLog = "查岗";
                    break;
                default:
                    break;
            }
            return result;
        }
        /// <summary>
        /// 处理命令2，对用户信息传输装置上报的数据进行解析
        /// </summary>
        /// <param name="commandType"></param>
        /// <param name="messageEntity"></param>
        /// <returns>返回 处理结果</returns>
        public static Data2 handler2(int commandType, GBMessageEntity messageEntity)
        {
            //string result = "";
            Data2 result = new Data2();
            result.command = commandType;
            switch (commandType)
            {
                case -1://空内容，“无锡蓝天”厂家心跳
                    result.type = 3;
                    result.msg = "空内容（无锡-心跳）";
                    result.msgLog = "空内容（无锡-心跳）";
                    break;
                case 1://消防设施系统状态
                    result.type = 3;
                    result.msg = "消防设施系统状态";
                    result = GBProtocolAnalysis.Analysis1(result, messageEntity.MessageObjectContent, messageEntity.MessageObjectCount);
                    break;
                case 2://消防设施部件运行状态
                    result.type = 3;
                    result.msg = "消防设施部件运行状态";
                    result = GBProtocolAnalysis.Analysis2(result, messageEntity.MessageObjectContent, messageEntity.MessageObjectCount);
                    break;
                case 3://消防设施模拟量值
                    result.type = 3;
                    result.msg = "消防设施模拟量值";
                    result = GBProtocolAnalysis.Analysis3(result, messageEntity.MessageObjectContent, messageEntity.MessageObjectCount);
                    break;
                case 4://消防设施操作信息
                    result.type = 3;
                    result.msg = "消防设施操作信息";
                    result = GBProtocolAnalysis.Analysis4(result, messageEntity.MessageObjectContent, messageEntity.MessageObjectCount);
                    break;
                case 5://消防设施软件版本
                    result.type = 3;
                    result.msg = "消防设施软件版本";
                    result = GBProtocolAnalysis.Analysis5(result, messageEntity.MessageObjectContent, messageEntity.MessageObjectCount);
                    break;
                case 6://消防设施系统配置情况
                    result.type = 3;
                    result.msg = "消防设施系统配置情况";
                    if (messageEntity.MessageObjectContent.Length > 6 && messageEntity.MessageObjectCount == 1)//  单一
                    {
                        byte systemType = (byte)Convert.ToInt32(messageEntity.MessageObjectContent.Substring(0, 2), 16);
                        byte systemAdress = (byte)Convert.ToInt32(messageEntity.MessageObjectContent.Substring(2, 2), 16);
                        int iSystemSignLength = Convert.ToInt32(messageEntity.MessageObjectContent.Substring(4, 2), 16);
                        byte[] systemSign = ByteHelper.hexStringToBytes(messageEntity.MessageObjectContent.Substring(6, iSystemSignLength * 2));
                    }
                    break;
                case 7://消防设施部件配置情况
                    result.type = 3;
                    result.msg = "消防设施部件配置情况";
                    result = GBProtocolAnalysis.Analysis7(result, messageEntity.MessageObjectContent, messageEntity.MessageObjectCount);
                    break;
                case 8://消防设施系统时间
                    result.type = 3;
                    result.msg = "消防设施系统时间";
                    if (messageEntity.MessageObjectContent.Length == 20)//messageEntity.MessageObjectCount == 1 单一
                    {
                        byte systemType = (byte)Convert.ToInt32(messageEntity.MessageObjectContent.Substring(0, 2), 16);
                        byte systemAdress = (byte)Convert.ToInt32(messageEntity.MessageObjectContent.Substring(2, 2), 16);
                        byte[] time = ByteHelper.hexStringToBytes(messageEntity.MessageObjectContent.Substring(4, 12));
                    }
                    break;
                case 21://上传用户信息传输装置运行状态（通用）
                    result.msg = "用户信息传输装置运行状态";
                    result.type = 3;
                    result = GBProtocolAnalysis.Analysis21(result, messageEntity.MessageObjectContent, messageEntity.MessageObjectCount);
                    break;
                case 24://上传用户信息传输装置操作信息（通用）
                    result.msg = "用户信息传输装置操作信息";
                    result.type = 3;
                    result = GBProtocolAnalysis.Analysis24(result, messageEntity.MessageObjectContent, messageEntity.MessageObjectCount);
                    break;
                case 25://上传用户信息传输装置软件版本
                    result.type = 3;
                    result.msg = "用户信息传输装置软件版本";
                    if (messageEntity.MessageObjectContent.Length == 4 && messageEntity.MessageObjectCount == 1)// 统一
                    {
                        byte[] time = ByteHelper.hexStringToBytes(messageEntity.MessageObjectContent.Substring(0, 4));
                    }
                    break;
                case 26://上传用户信息传输装置配置情况（通用，NTE-FANT6801专用配置格式）
                    result.type = 3;
                    result.msg = "上传用户信息传输装置配置情况";
                    result = FANTAnalysis.Analysis26(result, messageEntity.MessageObjectContent, messageEntity.MessageObjectCount);
                    break;
                case 28://上传用户信息传输装置系统时间

                    break;
                case 30://心跳（NTE-FANT6801 自定义）
                    result.msg = "FANT心跳";
                    result.type = 3;
                    result = FANTAnalysis.Analysis30(result, messageEntity.MessageObjectContent, messageEntity.MessageObjectCount);

                    break;
                default://未解析或类型错误
                    break;
            }
            return result;
        }


        #region handler2的具体解析与远程接口调用

        #endregion

        #region sendToApp
        /// <summary>
        /// 将解析后的数据发送给应用端（用队列来处理）
        /// </summary>
        /// <param name="url">应用端数据接口</param>
        /// <param name="entity">数据</param>
        public static void sendToApp(string url, string entity)
        {
            if (!string.IsNullOrWhiteSpace(url))
            {
                HttpWebRequest httpRequest = (HttpWebRequest)WebRequest.CreateDefault(new Uri(url));
                HttpWebResponse httpResponse = null;
                try
                {
                    httpRequest.Method = "POST";
                    httpRequest.ContentType = "application/x-www-form-urlencoded";
                    httpRequest.Timeout = 2000;
                    byte[] data = Encoding.UTF8.GetBytes(entity);
                    httpRequest.ContentLength = data.Length;
                    httpRequest.GetRequestStream().Write(data, 0, data.Length);
                    httpResponse = (HttpWebResponse)httpRequest.GetResponse();
                }
                catch (Exception ex)
                {
                    Console.WriteLine("HTTP Error " + ex.Message);
                }
                finally
                {
                    if (httpResponse != null) httpResponse.Close();
                    httpRequest.Abort();
                    Console.WriteLine("HTTP Close");
                }
            }
        }
        #endregion

    }
}
