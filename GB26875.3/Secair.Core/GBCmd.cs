﻿using System;

namespace Secair.Core
{
    public class GBCmd
    {
        /// <summary>
        /// 设备唯一标示(目前为6字节源地址)
        /// </summary>
        private string _deviceId = "";
        public string deviceId
        {
            get { return _deviceId; }
            set { _deviceId = value; }
        }

        /// <summary>
        /// 设备商编码(由平台统一提供)
        /// </summary>
        private string _equipmentSupplierCode = "";
        public string equipmentSupplierCode
        {
            get { return _equipmentSupplierCode; }
            set { _equipmentSupplierCode = value; }
        }

        /// <summary>
        /// 协议类型（由平台和设备端协商）
        /// </summary>
        private int _command = 0;
        public int command
        {
            get { return _command; }
            set { _command = value; }
        }

        /// <summary>
        /// 设备上报报文的时间
        /// </summary>
        private DateTime _eventTime = DateTime.MinValue;
        public DateTime eventTime
        {
            get { return _eventTime; }
            set { _eventTime = value; }
        }

        /// <summary>
        /// 原始报文
        /// </summary>
        private string _messageData = "";
        public string messageData
        {
            get { return _messageData; }
            set { _messageData = value; }
        }

        /// <summary>
        /// 解析数据
        /// </summary>
        private Object _formatObject = null;
        public Object formatObject
        {
            get { return _formatObject; }
            set { _formatObject = value; }
        }
    }
}