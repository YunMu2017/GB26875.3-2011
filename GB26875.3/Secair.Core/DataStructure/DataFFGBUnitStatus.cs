﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Secair.Core.DataStructure
{
    public class DataFFGBUnitStatus
    {
        /// <summary>
        /// 状态
        /// </summary>
        private string status = "测试状态";
        public string Status
        {
            get { return status; }
            set { status = value; }
        }

        /// <summary>
        /// 火警
        /// </summary>
        private string fireAlarm = "无火警";
        public string FireAlarm
        {
            get { return fireAlarm; }
            set { fireAlarm = value; }
        }

        /// <summary>
        /// 故障
        /// </summary>
        private string fault = "无故障";
        public string Fault
        {
            get { return fault; }
            set { fault = value; }
        }

        // <summary>
        /// 屏蔽
        /// </summary>
        private string mask = "无屏蔽";
        public string Mask
        {
            get { return mask; }
            set { mask = value; }
        }

        // <summary>
        /// 监管
        /// </summary>
        private string control = "无监管";
        public string Control
        {
            get { return control; }
            set { control = value; }
        }

        // <summary>
        /// 启动
        /// </summary>
        private string startUp = "停止";
        public string StartUp
        {
            get { return startUp; }
            set { startUp = value; }
        }

        // <summary>
        /// 反馈
        /// </summary>
        private string feedback = "无反馈";
        public string Feedback
        {
            get { return feedback; }
            set { feedback = value; }
        }

        // <summary>
        /// 延时
        /// </summary>
        private string delayed = "未延时";
        public string Delayed
        {
            get { return delayed; }
            set { delayed = value; }
        }

        /// <summary>
        /// 电源正常
        /// </summary>
        private string power = "电源正常";
        public string Power
        {
            get { return power; }
            set { power = value; }
        }

        /// <summary>
        /// 预留1
        /// </summary>
        private string bak1 = "预留";
        public string Bak1
        {
            get { return bak1; }
            set { bak1 = value; }
        }

        /// <summary>
        /// 预留2
        /// </summary>
        private string bak2 = "预留";
        public string Bak2
        {
            get { return bak2; }
            set { bak2 = value; }
        }

        /// <summary>
        /// 预留3
        /// </summary>
        private string bak3 = "预留";
        public string Bak3
        {
            get { return bak3; }
            set { bak3 = value; }
        }

        /// <summary>
        /// 预留2
        /// </summary>
        private string bak4 = "预留";
        public string Bak4
        {
            get { return bak4; }
            set { bak4 = value; }
        }

        /// <summary>
        /// 预留5
        /// </summary>
        private string bak5 = "预留";
        public string Bak5
        {
            get { return bak5; }
            set { bak5 = value; }
        }

        /// <summary>
        /// 预留6
        /// </summary>
        private string bak6 = "预留";
        public string Bak6
        {
            get { return bak6; }
            set { bak6 = value; }
        }

        /// <summary>
        /// 预留7
        /// </summary>
        private string bak7 = "预留";
        public string Bak7
        {
            get { return bak7; }
            set { bak7 = value; }
        }
    }
}
