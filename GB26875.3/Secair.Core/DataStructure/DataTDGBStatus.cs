﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Secair.Core.DataStructure
{
    public class DataTDGBStatus
    {
        /// <summary>
        /// 状态
        /// </summary>
        private string status = "测试状态";
        public string Status
        {
            get { return status; }
            set { status = value; }
        }

        /// <summary>
        /// 火警
        /// </summary>
        private string fireAlarm = "无火警";
        public string FireAlarm
        {
            get { return fireAlarm; }
            set { fireAlarm = value; }
        }

        /// <summary>
        /// 故障
        /// </summary>
        private string fault = "无故障";
        public string Fault
        {
            get { return fault; }
            set { fault = value; }
        }

        /// <summary>
        /// 主电
        /// </summary>
        private string mainPower = "主电正常";
        public string MainPower
        {
            get { return mainPower; }
            set { mainPower = value; }
        }

        /// <summary>
        /// 备电
        /// </summary>
        private string standbyPower = "备电正常";
        public string StandbyPower
        {
            get { return standbyPower; }
            set { standbyPower = value; }
        }

        /// <summary>
        /// 通信信道
        /// </summary>
        private string communicationChannel = "通信信道正常";
        public string CommunicationChannel
        {
            get { return communicationChannel; }
            set { communicationChannel = value; }
        }

        /// <summary>
        /// 连接线路
        /// </summary>
        private string connection = "检测连接线路正常";
        public string Connection
        {
            get { return connection; }
            set { connection = value; }
        }

        /// <summary>
        /// 预留1
        /// </summary>
        private string bak = "预留";
        public string Bak
        {
            get { return bak; }
            set { bak = value; }
        }
    }
}
