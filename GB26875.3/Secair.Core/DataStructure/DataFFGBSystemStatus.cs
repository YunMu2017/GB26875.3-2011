﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Secair.Core.DataStructure
{
    public class DataFFGBSystemStatus
    {
        /// <summary>
        /// 状态
        /// </summary>
        private string status = "测试状态";
        public string Status
        {
            get { return status; }
            set { status = value; }
        }

        /// <summary>
        /// 火警
        /// </summary>
        private string fireAlarm = "无火警";
        public string FireAlarm
        {
            get { return fireAlarm; }
            set { fireAlarm = value; }
        }

        /// <summary>
        /// 故障
        /// </summary>
        private string fault = "无故障";
        public string Fault
        {
            get { return fault; }
            set { fault = value; }
        }

        // <summary>
        /// 屏蔽
        /// </summary>
        private string mask = "无屏蔽";
        public string Mask
        {
            get { return mask; }
            set { mask = value; }
        }

        // <summary>
        /// 监管
        /// </summary>
        private string control = "无监管";
        public string Control
        {
            get { return control; }
            set { control = value; }
        }

        // <summary>
        /// 启动
        /// </summary>
        private string startUp = "停止";
        public string StartUp
        {
            get { return startUp; }
            set { startUp = value; }
        }

        // <summary>
        /// 反馈
        /// </summary>
        private string feedback = "无反馈";
        public string Feedback
        {
            get { return feedback; }
            set { feedback = value; }
        }

        // <summary>
        /// 延时
        /// </summary>
        private string delayed = "未延时";
        public string Delayed
        {
            get { return delayed; }
            set { delayed = value; }
        }

        /// <summary>
        /// 主电
        /// </summary>
        private string mainPower = "主电正常";
        public string MainPower
        {
            get { return mainPower; }
            set { mainPower = value; }
        }

        /// <summary>
        /// 备电
        /// </summary>
        private string standbyPower = "备电正常";
        public string StandbyPower
        {
            get { return standbyPower; }
            set { standbyPower = value; }
        }

        /// <summary>
        /// 总线
        /// </summary>
        private string bus = "总线正常";
        public string Bus
        {
            get { return bus; }
            set { bus = value; }
        }

        /// <summary>
        /// 手动
        /// </summary>
        private string manual = "自动状态";
        public string Manual
        {
            get { return manual; }
            set { manual = value; }
        }

        /// <summary>
        /// 配置改变
        /// </summary>
        private string configurationChange = "无配置改变";
        public string ConfigurationChange
        {
            get { return configurationChange; }
            set { configurationChange = value; }
        }

        /// <summary>
        /// 复位
        /// </summary>
        private string reset = "正常";
        public string Reset
        {
            get { return reset; }
            set { reset = value; }
        }

        /// <summary>
        /// 预留1
        /// </summary>
        private string bak1 = "预留";
        public string Bak1
        {
            get { return bak1; }
            set { bak1 = value; }
        }

        /// <summary>
        /// 预留2
        /// </summary>
        private string bak2 = "预留";
        public string Bak2
        {
            get { return bak2; }
            set { bak2 = value; }
        }
    }
}
