﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Secair.Core.DataStructure
{
    public class DataTDGBOperationInformation
    {
        /// <summary>
        /// 复位
        /// </summary>
        public string Reset { set; get; }

        /// <summary>
        /// 消音
        /// </summary>
        public string Silencing { set; get; }

        /// <summary>
        /// 手动报警
        /// </summary>
        public string ManualAlarm { set; get; }

        /// <summary>
        /// 警情消除
        /// </summary>
        public string EliminateAlarm { set; get; }

        /// <summary>
        /// 自检
        /// </summary>
        public string SelfChecking { set; get; }

        /// <summary>
        /// 查岗应答
        /// </summary>
        public string Response { set; get; }

        /// <summary>
        /// 测试
        /// </summary>
        public string Test { set; get; }

        /// <summary>
        /// 预留
        /// </summary>
        public string Bak { set; get; }
    }
}
