﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Secair.Core
{
    public class ByteHelper
    {
        /// <summary>
        /// 字节数组转16进制字符串(全部)
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public static string bytesToHexString(byte[] bytes)
        {
            string returnStr = "";
            if (bytes != null)
            {
                for (int i = 0; i < bytes.Length; i++)
                {
                    returnStr += bytes[i].ToString("X2");
                }
            }
            return returnStr;
        }

        /// <summary>
        /// 字节数组转16进制字符串(指定部分)
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public static string bytesToHexString(byte[] bytes, int iStart, int iLength)
        {
            string returnStr = "";
            try
            {
                if (bytes != null)
                {
                    for (int i = 0; i < iLength; i++)
                    {
                        returnStr += bytes[i + iStart].ToString("X2");
                    }
                }
            }
            catch { }
            return returnStr;
        }

        /// <summary>
        /// 字符串转16进制字节数组
        /// </summary>
        /// <param name="hexString"></param>
        /// <returns></returns>
        public static byte[] hexStringToBytes(string hexString)
        {
            hexString = hexString.Replace(" ", "");
            if ((hexString.Length % 2) != 0)
                hexString += " ";
            byte[] returnBytes = new byte[hexString.Length / 2];
            for (int i = 0; i < returnBytes.Length; i++)
                returnBytes[i] = Convert.ToByte(hexString.Substring(i * 2, 2), 16);
            return returnBytes;
        }

        /// <summary>
        /// 16进制字符串转用户编号
        /// </summary>
        /// <param name="hexString"></param>
        /// <returns></returns>
        public static int hexStringToIAdress(string hexString)
        {
            Byte[] DestinationAddress = new byte[] { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            byte[] bytDat = hexStringToBytes(hexString);
            Array.Copy(bytDat, 0, DestinationAddress, 0, 6);
            ulong iAdress = BitConverter.ToUInt64(DestinationAddress, 0);
            return Convert.ToInt32(iAdress);
        }
    }
}
