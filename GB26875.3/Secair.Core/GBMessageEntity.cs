﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Secair.Core
{
    /// <summary>
    /// 报文实体
    /// </summary>
    public class GBMessageEntity
    {
        /// <summary>
        /// 启动符 @@  0x40 0x40
        /// </summary>
        public string StartFlag { set; get; }

        #region Header
        /// <summary>
        /// 业务流水号
        /// </summary>
        public string SerialNumber { set; get; }

        /// <summary>
        /// 版本号
        /// </summary>
        public string Version { set; get; }

        /// <summary>
        /// 时间标签
        /// </summary>
        public string MessageDateTime { set; get; }

        /// <summary>
        /// 源地址
        /// </summary>
        public string Source { set; get; }

        /// <summary>
        /// 目标地址
        /// </summary>
        public string Target { set; get; }

        /// <summary>
        /// 应用数据单元长度
        /// </summary>
        public int DataUnitLength { set; get; }

        /// <summary>
        /// 命令字节
        /// </summary>
        public int Command { set; get; }
        #endregion

        #region DataUnit
        /// <summary>
        /// 类型标识符
        /// </summary>
        public int Type { set; get; }

        /// <summary>
        /// 信息对象数目
        /// </summary>
        public int MessageObjectCount { set; get; }

        /// <summary>
        /// 信息对象体
        /// </summary>
        public string MessageObjectContent { set; get; }
        #endregion

        /// <summary>
        /// 校验和
        /// </summary>
        public string CheckSum { set; get; }

        /// <summary>
        /// 结束符 ## 0x23 0x23
        /// </summary>
        public string EndFlag { set; get; }
    }
}
