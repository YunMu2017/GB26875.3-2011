﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Secair.Core.Tool
{
    public class GBProtocolAnalysis
    {
        #region 国标协议格式校验
        /// <summary>
        /// 国标协议格式总体校验(适配同时传多条协议)
        /// </summary>
        public static bool CheckGBProtocolAll(byte[] bytes)
        {
            if (bytes.Length < 30)
                return false;
            if (bytes[0] != 0x40 || bytes[1] != 0x40 || bytes[bytes.Length - 1] != 0x23 || bytes[bytes.Length - 2] != 0x23)
                return false;
            byte[] temp = new byte[bytes.Length - 5];
            Buffer.BlockCopy(bytes, 2, temp, 0, bytes.Length - 5);
            return GBCheck.CS(temp) == bytes[bytes.Length - 3] ? true : false;
        }

        public static string[] CheckGBProtocol(byte[] bytes)
        {
            string[] result = new string[0];
            if (bytes.Length < 30)
                return result;
            if (bytes[0] != 0x40 || bytes[1] != 0x40 || bytes[bytes.Length - 1] != 0x23 || bytes[bytes.Length - 2] != 0x23)
                return result;
            string[] strTemps = Secair.Core.ByteHelper.bytesToHexString(bytes).Split(new string[] { "23234040" }, StringSplitOptions.RemoveEmptyEntries);
            return strTemps.Length > 0 ? strTemps : result;
        }

        public static bool CheckGBProtocolCS(byte[] bytes)
        {
            byte[] temp = new byte[bytes.Length - 5];
            Buffer.BlockCopy(bytes, 2, temp, 0, bytes.Length - 5);
            return GBCheck.CS(temp) == bytes[bytes.Length - 3] ? true : false;
        }
        #endregion

        #region 国标协议1
        /// <summary>
        /// 国标1协议
        /// </summary>
        public static Data.Data2 Analysis1(Data.Data2 _data2, string _messageObjectContent, int _messageObjectCount)
        {
            Data.Data2 result = _data2;
            result.msgLog = _messageObjectContent.Length.ToString() + ",未解析";
            if (_messageObjectCount <= 102)
            {
                if (_messageObjectContent.Length == 20 * _messageObjectCount)
                {
                    string temp = System.Text.RegularExpressions.Regex.Replace(_messageObjectContent, @"(\w{20})", "$1-").Trim('-');
                    string[] temps = temp.Split('-');
                    Data.DataGB1[] datas = new Data.DataGB1[temps.Length];
                    int i = 0;
                    foreach (string s in temps)
                    {
                        Data.DataGB1 data = new Data.DataGB1
                        {
                            SystemType = Tool.GBStructureAnalysis.AnalysisFireFacilitiesGBSystemType(Convert.ToInt32(s.Substring(0, 2), 16)),
                            SystemAdress = Convert.ToInt32(s.Substring(2, 2), 16),
                            Status = Tool.GBStructureAnalysis.AnalysisFireFacilitiesGBSystemStatus(ByteHelper.hexStringToBytes(s.Substring(4, 4))),
                            Time = Tool.GBStructureAnalysis.AnalysisDateTimeGB(ByteHelper.hexStringToBytes(s.Substring(8, 12)))
                        };
                        datas[i] = data;
                        i++;
                    }
                    result.msgLog = Log.LogGB.LogGB1(datas);
                    result.data = datas;
                }
            }
            return result;
        }
        #endregion

        #region 国标协议2
        /// <summary>
        /// 国标2协议
        /// </summary>
        public static Data.Data2 Analysis2(Data.Data2 _data2, string _messageObjectContent, int _messageObjectCount)
        {
            Data.Data2 result = _data2;
            result.msgLog = _messageObjectContent.Length.ToString() + ",未解析";
            if (_messageObjectContent.Length == 92 * _messageObjectCount)
            {
                string temp = System.Text.RegularExpressions.Regex.Replace(_messageObjectContent, @"(\w{92})", "$1-").Trim('-');
                string[] temps = temp.Split('-');
                Data.DataGB2[] datas = new Data.DataGB2[temps.Length];
                int i = 0;
                foreach (string s in temps)
                {
                    Data.DataGB2 data = new Data.DataGB2
                    {
                        SystemType = Tool.GBStructureAnalysis.AnalysisFireFacilitiesGBSystemType(Convert.ToInt32(s.Substring(0, 2), 16)),
                        SystemAdress = Convert.ToInt32(s.Substring(2, 2), 16),
                        UnitType = Tool.GBStructureAnalysis.AnalysisFireFacilitiesGBUnitType(Convert.ToInt32(s.Substring(4, 2), 16)),
                        UnitAdress = Tool.GBStructureAnalysis.AnalysisFireFacilitiesGBAdress(ByteHelper.hexStringToBytes(s.Substring(6, 8))),
                        UnitStatus = Tool.GBStructureAnalysis.AnalysisFireFacilitiesGBUnitStatus(ByteHelper.hexStringToBytes(s.Substring(14, 4))),
                        UnitInstructions = s.Substring(18, 62),
                        Time = Tool.GBStructureAnalysis.AnalysisDateTimeGB(ByteHelper.hexStringToBytes(_messageObjectContent.Substring(80, 12)))
                    };
                    datas[i] = data;
                    i++;
                }
                result.msgLog = Log.LogGB.LogGB2(datas);
                result.data = datas;
            }
            return result;
        }
        #endregion

        #region 国标协议3
        /// <summary>
        /// 国标3协议
        /// </summary>
        public static Data.Data2 Analysis3(Data.Data2 _data2, string _messageObjectContent, int _messageObjectCount)
        {
            Data.Data2 result = _data2;
            result.msgLog = _messageObjectContent.Length.ToString() + ",未解析";
            if (_messageObjectContent.Length == 32 * _messageObjectCount)
            {
                string temp = System.Text.RegularExpressions.Regex.Replace(_messageObjectContent, @"(\w{32})", "$1-").Trim('-');
                string[] temps = temp.Split('-');
                Data.DataGB3[] datas = new Data.DataGB3[temps.Length];
                int i = 0;
                foreach (string s in temps)
                {
                    Data.DataGB3 data = new Data.DataGB3
                    {
                        SystemType = Tool.GBStructureAnalysis.AnalysisFireFacilitiesGBSystemType(Convert.ToInt32(s.Substring(0, 2), 16)),
                        SystemAdress = Convert.ToInt32(s.Substring(2, 2), 16),
                        UnitType = Tool.GBStructureAnalysis.AnalysisFireFacilitiesGBUnitType(Convert.ToInt32(s.Substring(4, 2), 16)),
                        UnitAdress = s.Substring(6, 8),
                        AnalogType = Tool.GBStructureAnalysis.AnalysisFireFacilitiesGBUnitType(Convert.ToInt32(s.Substring(14, 2), 16)),
                        AnalogValue = s.Substring(16, 4),
                        Time = Tool.GBStructureAnalysis.AnalysisDateTimeGB(ByteHelper.hexStringToBytes(_messageObjectContent.Substring(20, 12)))
                    };
                    datas[i] = data;
                    i++;
                }
                result.msgLog = Log.LogGB.LogGB3(datas);
                result.data = datas;
            }
            return result;
        }
        #endregion

        #region 国标协议4
        /// <summary>
        /// 国标4协议
        /// </summary>
        public static Data.Data2 Analysis4(Data.Data2 _data2, string _messageObjectContent, int _messageObjectCount)
        {
            Data.Data2 result = _data2;
            result.msgLog = _messageObjectContent.Length.ToString() + ",未解析";
            if (_messageObjectContent.Length == 20 * _messageObjectCount)
            {
                string temp = System.Text.RegularExpressions.Regex.Replace(_messageObjectContent, @"(\w{20})", "$1-").Trim('-');
                string[] temps = temp.Split('-');
                Data.DataGB4[] datas = new Data.DataGB4[temps.Length];
                int i = 0;
                foreach (string s in temps)
                {
                    Data.DataGB4 data = new Data.DataGB4
                    {
                        SystemType = Tool.GBStructureAnalysis.AnalysisFireFacilitiesGBSystemType(Convert.ToInt32(s.Substring(0, 2), 16)),
                        SystemAdress = Convert.ToInt32(s.Substring(2, 2), 16),
                        OpeInfo = Tool.GBStructureAnalysis.AnalysisFireFacilitiesGBOperationInformation(Convert.ToByte(s.Substring(4, 2), 16)),
                        OpeNo = s.Substring(6, 2),
                        Time = Tool.GBStructureAnalysis.AnalysisDateTimeGB(ByteHelper.hexStringToBytes(_messageObjectContent.Substring(8, 12)))
                    };
                    datas[i] = data;
                    i++;
                }
                result.msgLog = Log.LogGB.LogGB4(datas);
                result.data = datas;
            }
            return result;
        }
        #endregion

        #region 国标协议5
        /// <summary>
        /// 国标5协议
        /// </summary>
        public static Data.Data2 Analysis5(Data.Data2 _data2, string _messageObjectContent, int _messageObjectCount)
        {
            Data.Data2 result = _data2;
            result.msgLog = _messageObjectContent.Length.ToString() + ",未解析";
            if (_messageObjectContent.Length == 8 * _messageObjectCount)
            {
                string temp = System.Text.RegularExpressions.Regex.Replace(_messageObjectContent, @"(\w{8})", "$1-").Trim('-');
                string[] temps = temp.Split('-');
                Data.DataGB5[] datas = new Data.DataGB5[temps.Length];
                int i = 0;
                foreach (string s in temps)
                {
                    Data.DataGB5 data = new Data.DataGB5
                    {
                        SystemType = Tool.GBStructureAnalysis.AnalysisFireFacilitiesGBSystemType(Convert.ToInt32(s.Substring(0, 2), 16)),
                        SystemAdress = Convert.ToInt32(s.Substring(2, 2), 16),
                        MajorVersion = Convert.ToInt32(s.Substring(4, 2), 16),
                        MinorVersion = Convert.ToInt32(s.Substring(6, 2), 16),
                    };
                    datas[i] = data;
                    i++;
                }
                result.msgLog = Log.LogGB.LogGB5(datas);
                result.data = datas;
            }
            return result;
        }
        #endregion

        #region 国标协议7
        /// <summary>
        /// 国标7协议
        /// </summary>
        public static Data.Data2 Analysis7(Data.Data2 _data2, string _messageObjectContent, int _messageObjectCount)
        {
            Data.Data2 result = _data2;
            result.msgLog = _messageObjectContent.Length.ToString() + ",未解析";
            if (_messageObjectContent.Length == 76 * _messageObjectCount)
            {
                string temp = System.Text.RegularExpressions.Regex.Replace(_messageObjectContent, @"(\w{76})", "$1-").Trim('-');
                string[] temps = temp.Split('-');
                Data.DataGB7[] datas = new Data.DataGB7[temps.Length];
                int i = 0;
                foreach (string s in temps)
                {
                    Data.DataGB7 data = new Data.DataGB7
                    {
                        SystemType = Tool.GBStructureAnalysis.AnalysisFireFacilitiesGBSystemType(Convert.ToInt32(s.Substring(0, 2), 16)),
                        SystemAdress = Convert.ToInt32(s.Substring(2, 2), 16),
                        UnitType = Tool.GBStructureAnalysis.AnalysisFireFacilitiesGBUnitType(Convert.ToInt32(s.Substring(4, 2), 16)),
                        UnitAdress = s.Substring(6, 8),
                        UnitInstructions = s.Substring(14, 62)
                    };
                    datas[i] = data;
                    i++;
                }
                result.msgLog = Log.LogGB.LogGB7(datas);
                result.data = datas;
            }
            return result;
        }
        #endregion

        #region 国标协议8
        /// <summary>
        /// 国标8协议
        /// </summary>
        public static Data.Data2 Analysis8(Data.Data2 _data2, string _messageObjectContent, int _messageObjectCount)
        {
            Data.Data2 result = _data2;
            result.msgLog = _messageObjectContent.Length.ToString() + ",未解析";
            if (_messageObjectContent.Length == 16)
            {
                Data.DataGB8 data = new Data.DataGB8
                {
                    SystemType = Tool.GBStructureAnalysis.AnalysisFireFacilitiesGBSystemType(Convert.ToInt32(_messageObjectContent.Substring(0, 2), 16)),
                    SystemAdress = Convert.ToInt32(_messageObjectContent.Substring(2, 2), 16),
                    Time = Tool.GBStructureAnalysis.AnalysisDateTimeGB(ByteHelper.hexStringToBytes(_messageObjectContent.Substring(4, 12)))
                };
                result.msgLog = Log.LogGB.LogGB8(data);
                result.data = data;
            }
            return result;
        }
        #endregion

        #region 国标协议21
        /// <summary>
        /// 国标21协议
        /// </summary>
        public static Data.Data2 Analysis21(Data.Data2 _data2, string _messageObjectContent, int _messageObjectCount)
        {
            Data.Data2 result = _data2;
            result.msgLog = _messageObjectContent.Length.ToString() + ",未解析";
            if (_messageObjectCount == 1)//messageEntity.MessageObjectCount == 1 单一
            {
                if (_messageObjectContent.Length == 14)
                {
                    Data.DataGB21 data = new Data.DataGB21
                    {
                        Status = Tool.GBStructureAnalysis.AnalysisTransmissionDeviceStatusGB((byte)Convert.ToInt32(_messageObjectContent.Substring(0, 2), 16)),
                        StatusTime = Tool.GBStructureAnalysis.AnalysisDateTimeGB(ByteHelper.hexStringToBytes(_messageObjectContent.Substring(2, 12)))
                    };
                    result.msgLog = Log.LogGB.LogGB21(data);
                    result.data = data;
                }
            }
            else
            {
                if (_messageObjectContent.Length == 14 * _messageObjectCount)
                {
                    string temp = System.Text.RegularExpressions.Regex.Replace(_messageObjectContent, @"(\w{14})", "$1-").Trim('-');
                }
            }
            return result;
        }
        #endregion

        #region 国标协议24
        /// <summary>
        /// 国标24协议
        /// </summary>
        public static Data.Data2 Analysis24(Data.Data2 _data2, string _messageObjectContent, int _messageObjectCount)
        {
            Data.Data2 result = _data2;
            result.msgLog = _messageObjectContent.Length.ToString() + ",未解析";
            if (_messageObjectCount == 1)//messageEntity.MessageObjectCount == 1 单一
            {
                if (_messageObjectContent.Length == 16)
                {
                    Data.DataGB24 data = new Data.DataGB24
                    {
                        Info = Tool.GBStructureAnalysis.AnalysisTransmissionDeviceOpeGB((byte)Convert.ToInt32(_messageObjectContent.Substring(0, 2), 16)),
                        OpeNo = (byte)Convert.ToInt32(_messageObjectContent.Substring(2, 2), 16),
                        OpeTime = Tool.GBStructureAnalysis.AnalysisDateTimeGB(ByteHelper.hexStringToBytes(_messageObjectContent.Substring(4, 12)))
                    };
                    result.msgLog = Log.LogGB.LogGB24(data);
                    result.data = data;
                }
            }
            else
            {
                if (_messageObjectContent.Length == 16 * _messageObjectCount)
                {
                    string temp = System.Text.RegularExpressions.Regex.Replace(_messageObjectContent, @"(\w{16})", "$1-").Trim('-');
                    string[] temps = temp.Split('-');
                    Data.DataGB24[] datas = new Data.DataGB24[temps.Length];
                    int i = 0;
                    foreach (string s in temps)
                    {
                        Data.DataGB24 data = new Data.DataGB24
                        {
                            Info = Tool.GBStructureAnalysis.AnalysisTransmissionDeviceOpeGB((byte)Convert.ToInt32(s.Substring(0, 2), 16)),
                            OpeNo = (byte)Convert.ToInt32(s.Substring(2, 2), 16),
                            OpeTime = Tool.GBStructureAnalysis.AnalysisDateTimeGB(ByteHelper.hexStringToBytes(s.Substring(4, 12)))
                        };
                        datas[i] = data;
                        i++;
                    }
                    result.msgLog = Log.LogGB.LogGB24(datas);
                    result.data = datas;
                }
            }
            return result;
        }
        #endregion

        #region 国标协议90
        /// <summary>
        /// 国标1协议
        /// </summary>
        public static Data.Data1 Analysis90(Data.Data1 _data1, string _messageObjectContent)
        {
            Data.Data1 result = _data1;
            result.msgLog = _messageObjectContent.Length.ToString() + ",未解析";
            if (_messageObjectContent.Length == 12)
            {
                Data.DataGB90 data = new Data.DataGB90
                {
                    Time = Tool.GBStructureAnalysis.AnalysisDateTimeGB(ByteHelper.hexStringToBytes(_messageObjectContent))
                };
                result.msgLog = Log.LogGB.LogGB90(data);
                result.data = data;
            }
            return result;
        }
        #endregion
    }
}
