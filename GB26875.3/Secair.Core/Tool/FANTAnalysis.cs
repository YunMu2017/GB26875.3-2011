﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Secair.Core.Tool
{
    public class FANTAnalysis
    {
        #region 法安通 自定义结构
        /// <summary>
        /// 最大设备ID长度 
        /// </summary>
        public const int MaxDeviceIdLength = 13;

        /// <summary>
        /// 最大用户编码长度 
        /// </summary>
        public const int MaxUserCodeLength = 9;

        /// <summary>
        /// 最大SIM ID长度
        /// </summary>
        public const int MaxSimIdLength = 19;

        /// <summary>
        /// 配置文本数据长度
        /// </summary>
        public const int DataSettingLength = 1;

        /// <summary>
        /// CAN 协议长度
        /// </summary>
        public const int CanProtocolLength = 3;

        /// <summary>
        /// RS232 协议长度
        /// </summary>
        public const int RS232ProtocolLength = 3;

        /// <summary>
        /// RS485 协议长度
        /// </summary>
        public const int RS485ProtocolLength = 3;

        /// <summary>
        /// 版本号 长度
        /// </summary>
        public const int EditionLength = 3;
        #endregion

        /// <summary>
        /// 自定义协议30 心跳
        /// </summary>
        public static Data.Data2 Analysis30(Data.Data2 _data2, string _messageObjectContent, int _messageObjectCount)
        {
            Data.Data2 result = _data2;
            if (_messageObjectCount == 1)//单条
            {
                Data.DataFANT30 data = new Data.DataFANT30
                {
                    DeviceId = "",
                    UserNo = "",
                    CANProtocol = 0,
                    RS232Protocol = 0,
                    RS485Protocol = 0,
                    SignalQuality = 0,
                    SIMCCID = ""
                };
                byte[] temp = ByteHelper.hexStringToBytes(_messageObjectContent);
                byte[] tempX = temp;
                int iLoc = 0, iLocStart = 0;
                foreach (byte b in temp)
                {
                    iLoc++;
                    if (b == 0x00)
                    {
                        break;
                    }
                }
                if (iLoc <= MaxDeviceIdLength)
                {
                    //data.DeviceId = _messageObjectContent.Substring(0, iLoc * 2 - 2);
                    //data.DeviceId = Encoding.ASCII.GetString(temp, 0, iLoc - 1);
                    data.DeviceId = ByteHelper.bytesToHexString(temp, 0, iLoc - 1);
                }
                else
                {
                    return result;
                }
                temp = new byte[tempX.Length - iLoc];
                Buffer.BlockCopy(tempX, iLoc, temp, 0, temp.Length);
                iLocStart = iLoc;
                iLoc = 0;
                foreach (byte b in temp)
                {
                    iLoc++;
                    if (b == 0x00)
                    {
                        break;
                    }
                }
                if (iLoc <= MaxUserCodeLength)
                {
                    //result.id = Encoding.ASCII.GetString(temp, 0, iLoc);
                    /*if (temp[0] == 0x47 && temp[1] == 0x42)//GB开头
                        result.code = _messageObjectContent.Substring(iLocStart * 2, iLoc * 2 - 2);*/
                    if (temp[0] == 0x47 && temp[1] == 0x42)
                        data.UserNo = Encoding.ASCII.GetString(temp, 0, iLoc - 1);
                }
                else
                {
                    return result;
                }

                result.msgLog = Log.LogFANT.LogFANT30(data);
                result.data = data;
            }
            else { }//多条
            return result;
        }

        /// <summary>
        /// 协议26 上传用户信息传输装置配置情况（协议通用，部分内容自定义）
        /// </summary>
        public static Data.Data2 Analysis26(Data.Data2 _data2, string _messageObjectContent, int _messageObjectCount)
        {
            Data.Data2 result = _data2;
            if (_messageObjectCount == 1)//单条
            {
                Data.DataFANT26 data = new Data.DataFANT26
                {
                    DeviceId = "",
                    UserNo = "",
                    CANProtocol = 0,
                    RS232Protocol = 0,
                    RS485Protocol = 0,
                    SIMCCID = "",
                    Version = "",
                    Time = DateTime.MinValue
                };
                byte[] temp = ByteHelper.hexStringToBytes(_messageObjectContent);
                byte[] tempX = temp;
                int iLoc = 0, iLocStart = 0;
                foreach (byte b in temp)
                {
                    iLoc++;
                    if (b == 0x00)
                    {
                        break;
                    }
                }
                if (iLoc <= MaxDeviceIdLength)
                {
                    //data.DeviceId = _messageObjectContent.Substring(0, iLoc * 2 - 2);
                    data.DeviceId = Encoding.ASCII.GetString(temp, 0, iLoc - 1);
                }
                else
                {
                    return result;
                }
                temp = new byte[tempX.Length - iLoc];
                Buffer.BlockCopy(tempX, iLoc, temp, 0, temp.Length);
                iLocStart = iLoc;
                iLoc = 0;
                foreach (byte b in temp)
                {
                    iLoc++;
                    if (b == 0x00)
                    {
                        break;
                    }
                }
                if (iLoc <= MaxUserCodeLength)
                {
                    //result.id = Encoding.ASCII.GetString(temp, 0, iLoc);
                    /*if (temp[0] == 0x47 && temp[1] == 0x42)//GB开头
                        result.code = _messageObjectContent.Substring(iLocStart * 2, iLoc * 2 - 2);*/
                    if (temp[0] == 0x47 && temp[1] == 0x42)
                        data.UserNo = Encoding.ASCII.GetString(temp, 0, iLoc - 1);
                }
                else
                {
                    return result;
                }

                result.msgLog = Log.LogFANT.LogFANT26(data);
                result.data = data;
            }
            else { }//多条
            return result;
        }
        #region Tools

        #endregion
    }
}
