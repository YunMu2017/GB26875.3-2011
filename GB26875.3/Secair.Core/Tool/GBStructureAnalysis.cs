﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Secair.Core.DataStructure;

namespace Secair.Core.Tool
{
    public class GBStructureAnalysis
    {
        /// <summary>
        /// 时间标签数据 GB8.2.2
        /// </summary>
        public static DateTime AnalysisDateTimeGB(byte[] time)
        {
            DateTime _dt = time[4].Equals(0) | time[3].Equals(0) ? DateTime.MinValue : new DateTime(2000 + time[5], time[4], time[3], time[2], time[1], time[0]);
            return _dt;
        }


        #region Definition Table
        /// <summary>
        /// 系统类型 GB8.2.1.1
        /// </summary>
        public static string AnalysisFireFacilitiesGBSystemType(int type)
        {
            string result = "";
            switch (type)
            {
                case 0:
                    result = "通用";
                    break;
                case 1:
                    result = "火灾报警系统";
                    break;
                case 10:
                    result = "消防联动控制器";
                    break;
                case 11:
                    result = "消火栓系统";
                    break;
                case 12:
                    result = "自动喷水灭火系统";
                    break;
                case 13:
                    result = "气体灭火系统";
                    break;
                case 14:
                    result = "水喷雾灭火系统(泵启动方式)";
                    break;
                case 15:
                    result = "水喷雾灭火系统(压力容器启动方式)";
                    break;
                case 16:
                    result = "泡沫灭火系统";
                    break;
                case 17:
                    result = "干粉灭火系统";
                    break;
                case 18:
                    result = "防烟排烟系统";
                    break;
                case 19:
                    result = "防火门及卷帘系统";
                    break;
                case 20:
                    result = "消防电梯";
                    break;
                case 21:
                    result = "消防应急广播";
                    break;
                case 22:
                    result = "消防应急照明和疏散指示系统";
                    break;
                case 23:
                    result = "消防电源";
                    break;
                case 24:
                    result = "消防电话";
                    break;
                default:
                    result = "预留";
                    break;
            }
            return result;
        }

        /// <summary>
        /// 建筑消防设施部件类型 GB8.2.1.2
        /// </summary>
        public static string AnalysisFireFacilitiesGBUnitType(int type)
        {
            string result = "";
            switch (type)
            {
                case 0:
                    result = "通用";
                    break;
                case 1:
                    result = "火灾报警控制器";
                    break;
                case 10:
                    result = "可燃气体探铡器";
                    break;
                case 11:
                    result = "点型可燃气体探测器";
                    break;
                case 12:
                    result = "独立式可燃气体探测器";
                    break;
                case 13:
                    result = "线型可燃气体探测器";
                    break;
                case 16:
                    result = "电气火灾监控报警器";
                    break;
                case 17:
                    result = "剩余电流式电气火灾监控探测器";
                    break;
                case 18:
                    result = "测温式电气火灾监控探测器";
                    break;
                case 21:
                    result = "探测回路";
                    break;
                case 22:
                    result = "火灾显示盘";
                    break;
                case 23:
                    result = "手动火灾报警按钮";
                    break;
                case 24:
                    result = "消火栓按钮";
                    break;
                case 25:
                    result = "火灾探测器";
                    break;
                case 30:
                    result = "感温火灾探测器";
                    break;
                case 31:
                    result = "点型感温火灾探测器";
                    break;
                case 32:
                    result = "点型感温火灾探测器(S型)";
                    break;
                case 33:
                    result = "点型感温火灾探测器(R型)";
                    break;
                case 34:
                    result = "线型感温火灾探测器";
                    break;
                case 35:
                    result = "线型感温火灾探测器(S型)";
                    break;
                case 36:
                    result = "线型感温火灾探测器(R型)";
                    break;
                case 37:
                    result = "光纤感温火灾探测器";
                    break;
                case 40:
                    result = "感烟火灾探测器";
                    break;
                default:
                    result = "其他";
                    break;
            }
            return result;
        }

        /// <summary>
        /// 模拟量类型 GB8.2.1.3
        /// </summary>
        public static string AnalysisFireFacilitiesGBAnalogType(int type)
        {
            string result = "";
            switch (type)
            {
                case 0:
                    result = "未用";
                    break;
                case 1:
                    result = "事件计数";
                    break;
                case 2:
                    result = "高度";
                    break;
                case 3:
                    result = "温度";
                    break;
                case 4:
                    result = "压力（兆帕）";
                    break;
                case 5:
                    result = "压力（千帕）";
                    break;
                case 6:
                    result = "气体浓度";
                    break;
                case 7:
                    result = "时间";
                    break;
                case 8:
                    result = "电压";
                    break;
                case 9:
                    result = "电流";
                    break;
                case 10:
                    result = "流量";
                    break;
                case 11:
                    result = "风量";
                    break;
                case 12:
                    result = "风速";
                    break;
                default:
                    result = "预留";
                    break;
            }
            return result;
        }
        #endregion


        #region Structure
        /// <summary>
        /// 建筑消防设施系统状态 GB8.2.1.1
        /// </summary>
        public static DataFFGBSystemStatus AnalysisFireFacilitiesGBSystemStatus(byte[] datas)
        {
            DataFFGBSystemStatus result = new DataFFGBSystemStatus();
            byte data = datas[0];
            int temp = data & 0x01;
            if (temp.Equals(1))
                result.Status = "正常监视状态";
            temp = data >> 1 & 0x01;
            if (temp.Equals(1))
                result.FireAlarm = "火警";
            temp = data >> 2 & 0x01;
            if (temp.Equals(1))
                result.Fault = "故障";
            temp = data >> 3 & 0x01;
            if (temp.Equals(1))
                result.Mask = "屏蔽";
            temp = data >> 4 & 0x01;
            if (temp.Equals(1))
                result.Control = "监管";
            temp = data >> 5 & 0x01;
            if (temp.Equals(1))
                result.StartUp = "启动";
            temp = data >> 6 & 0x01;
            if (temp.Equals(1))
                result.Feedback = "反馈";
            temp = data >> 6 & 0x01;
            if (temp.Equals(1))
                result.Delayed = "延时状态";
            data = datas[1];
            temp = data & 0x01;
            if (temp.Equals(1))
                result.MainPower = "主电故障";
            temp = data >> 1 & 0x01;
            if (temp.Equals(1))
                result.StandbyPower = "备电故障";
            temp = data >> 2 & 0x01;
            if (temp.Equals(1))
                result.Bus = "总线故障";
            temp = data >> 3 & 0x01;
            if (temp.Equals(1))
                result.Manual = "手动状态";
            temp = data >> 4 & 0x01;
            if (temp.Equals(1))
                result.ConfigurationChange = "配置改变";
            temp = data >> 5 & 0x01;
            if (temp.Equals(1))
                result.Reset = "复位";
            return result;
        }

        /// <summary>
        /// 建筑消防设施部件状态 GB8.2.1.2
        /// </summary>
        public static DataFFGBUnitStatus AnalysisFireFacilitiesGBUnitStatus(byte[] datas)
        {
            DataFFGBUnitStatus result = new DataFFGBUnitStatus();
            byte data = datas[0];
            int temp = data & 0x01;
            if (temp.Equals(1))
                result.Status = "正常监视状态";
            temp = data >> 1 & 0x01;
            if (temp.Equals(1))
                result.FireAlarm = "火警";
            temp = data >> 2 & 0x01;
            if (temp.Equals(1))
                result.Fault = "故障";
            temp = data >> 3 & 0x01;
            if (temp.Equals(1))
                result.Mask = "屏蔽";
            temp = data >> 4 & 0x01;
            if (temp.Equals(1))
                result.Control = "监管";
            temp = data >> 5 & 0x01;
            if (temp.Equals(1))
                result.StartUp = "启动";
            temp = data >> 6 & 0x01;
            if (temp.Equals(1))
                result.Feedback = "反馈";
            temp = data >> 6 & 0x01;
            if (temp.Equals(1))
                result.Delayed = "延时状态";
            data = datas[1];
            temp = data & 0x01;
            if (temp.Equals(1))
                result.Power = "电源故障";
            return result;
        }

        /// <summary>
        /// 建筑消防设施部件回路、地址 GB8.2.1.2
        /// </summary>
        public static string AnalysisFireFacilitiesGBAdress(byte[] datas)
        {
            string result = (datas[3] * 256 + datas[2]).ToString() + "|" + (datas[1] * 256 + datas[0]).ToString();
            return result;
        }

        /// <summary>
        /// 建筑消防设施操作信息数据结构 GB8.2.1.4
        /// </summary>
        public static DataFFGBOperationInformation AnalysisFireFacilitiesGBOperationInformation(byte data)
        {
            DataFFGBOperationInformation result = new DataFFGBOperationInformation
            {
                Reset = "无操作",
                Silencing = "无操作",
                ManualAlarm = "无操作",
                EliminateAlarm = "无操作",
                SelfChecking = "无操作",
                Response = "无操作",
                Test = "无操作",
                Bak = ""
            };
            int temp = data & 0x01;
            if (temp.Equals(1))
                result.Reset = "复位";
            temp = data >> 1 & 0x01;
            if (temp.Equals(1))
                result.Silencing = "消音";
            temp = data >> 2 & 0x01;
            if (temp.Equals(1))
                result.ManualAlarm = "手动报警";
            temp = data >> 3 & 0x01;
            if (temp.Equals(1))
                result.EliminateAlarm = "警情消除";
            temp = data >> 4 & 0x01;
            if (temp.Equals(1))
                result.SelfChecking = "自检";
            temp = data >> 5 & 0x01;
            if (temp.Equals(1))
                result.Response = "查岗应答";
            temp = data >> 6 & 0x01;
            if (temp.Equals(1))
                result.Test = "测试";
            return result;
        }

        /// <summary>
        /// 用户信息传输装置运行状态 GB8.2.1.8
        /// </summary>
        public static DataTDGBStatus AnalysisTransmissionDeviceStatusGB(byte data)
        {
            DataTDGBStatus result = new DataTDGBStatus();
            int temp = data & 0x01;
            if (temp.Equals(1))
                result.Status = "正常监视状态";
            temp = data >> 1 & 0x01;
            if (temp.Equals(1))
                result.FireAlarm = "火警";
            temp = data >> 2 & 0x01;
            if (temp.Equals(1))
                result.Fault = "故障";
            temp = data >> 3 & 0x01;
            if (temp.Equals(1))
                result.MainPower = "主电故障";
            temp = data >> 4 & 0x01;
            if (temp.Equals(1))
                result.StandbyPower = "备电故障";
            temp = data >> 5 & 0x01;
            if (temp.Equals(1))
                result.CommunicationChannel = "与监控中心通信信道故障";
            temp = data >> 6 & 0x01;
            if (temp.Equals(1))
                result.Connection = "监测连接线路故障";
            return result;
        }

        /// <summary>
        /// 用户信息传输装置操作状态 GB8.2.1.9
        /// </summary>
        public static DataTDGBOperationInformation AnalysisTransmissionDeviceOpeGB(byte data)
        {
            DataTDGBOperationInformation result = new DataTDGBOperationInformation
            {
                Reset = "无操作",
                Silencing = "无操作",
                ManualAlarm = "无操作",
                EliminateAlarm = "无操作",
                SelfChecking = "无操作",
                Response = "无操作",
                Test = "无操作",
                Bak = ""
            };
            int temp = data & 0x01;
            if (temp.Equals(1))
                result.Reset = "复位";
            temp = data >> 1 & 0x01;
            if (temp.Equals(1))
                result.Silencing = "消音";
            temp = data >> 2 & 0x01;
            if (temp.Equals(1))
                result.ManualAlarm = "手动报警";
            temp = data >> 3 & 0x01;
            if (temp.Equals(1))
                result.EliminateAlarm = "警情消除";
            temp = data >> 4 & 0x01;
            if (temp.Equals(1))
                result.SelfChecking = "自检";
            temp = data >> 5 & 0x01;
            if (temp.Equals(1))
                result.Response = "查岗应答";
            temp = data >> 6 & 0x01;
            if (temp.Equals(1))
                result.Test = "测试";
            return result;
        }
        #endregion
    }
}
