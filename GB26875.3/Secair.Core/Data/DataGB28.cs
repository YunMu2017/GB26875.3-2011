﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Secair.Core.Data
{
    public class DataGB28
    {
        /// <summary>
        /// 用户传输装置的系统时间
        /// </summary>
        public DateTime Time { set; get; }
    }
}
