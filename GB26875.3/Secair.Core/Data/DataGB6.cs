﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Secair.Core.Data
{
    public class DataGB6
    {
        /// <summary>
        /// 系统类型-名称
        /// </summary>
        public string SystemType{ set; get; }

        /// <summary>
        /// 系统地址
        /// </summary>
        public int SystemAdress { set; get; }

        /// <summary>
        /// 系统说明长度
        /// </summary>
        public int SystemInstructionsLength { set; get; }

        /// <summary>
        /// 系统说明
        /// </summary>
        public string SystemInstructions{ set; get; }
    }
}
