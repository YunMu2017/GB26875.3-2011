﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Secair.Core.DataStructure;

namespace Secair.Core.Data
{
    public class DataGB21
    {
        /// <summary>
        /// 状态
        /// </summary>
        public DataTDGBStatus Status { set; get; }

        /// <summary>
        /// 状态发生时间
        /// </summary>
        public DateTime StatusTime { set; get; }
    }
}
