﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Secair.Core.Data
{
    public class DataGB25
    {
        /// <summary>
        /// 软件主版本号
        /// </summary>
        public int MajorVersion { set; get; }

        /// <summary>
        /// 软件次版本号
        /// </summary>
        public int MinorVersion { set; get; }
    }
}
