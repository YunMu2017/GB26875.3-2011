﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Secair.Core.DataStructure;

namespace Secair.Core.Data
{
    public class DataGB1
    {
        /// <summary>
        /// 系统类型-名称
        /// </summary>
        public string SystemType { set; get; }

        /// <summary>
        /// 系统地址
        /// </summary>
        public int SystemAdress { set; get; }

        /// <summary>
        /// 系统状态
        /// </summary>
        public DataFFGBSystemStatus Status { set; get; }

        /// <summary>
        /// 系统状态发生时间
        /// </summary>
        public DateTime Time { set; get; }
    }
}
