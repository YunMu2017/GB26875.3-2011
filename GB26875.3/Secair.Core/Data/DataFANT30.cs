﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Secair.Core.Data
{
    public class DataFANT30
    {
        /// <summary>
        /// 设备ID
        /// </summary>
        public string DeviceId{ set; get; }

        /// <summary>
        /// 用户编号
        /// </summary>
        public string UserNo { set; get; }

        /// <summary>
        /// CAN协议代号
        /// </summary>
        public int CANProtocol { set; get; }

        /// <summary>
        /// RS232协议代号
        /// </summary>
        public int RS232Protocol { set; get; }

        /// <summary>
        /// RS485协议代号
        /// </summary>
        public int RS485Protocol { set; get; }

        /// <summary>
        /// 信号质量
        /// </summary>
        public int SignalQuality { set; get; }

        /// <summary>
        /// SIM卡的CCID
        /// </summary>
        public string SIMCCID { set; get; }
    }
}
