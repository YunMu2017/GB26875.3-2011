﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Secair.Core.Data
{
    //读用户信息传输装置操作信息记录
    public class DataGB84
    {
        /// <summary>
        /// 查询操作信息记录数目
        /// </summary>
        public int Count { set; get; }

        /// <summary>
        /// 查询记录的指定起始时问
        /// </summary>
        public DateTime Time { set; get; }
    }
}
