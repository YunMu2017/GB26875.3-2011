﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Secair.Core.Data
{
    public class DataFANT26
    {
        /// <summary>
        /// 配置说明长度
        /// </summary>
        public int ConfigurationLength { set; get; }

        /// <summary>
        /// 设备ID
        /// </summary>
        public string DeviceId{ set; get; }

        /// <summary>
        /// 用户编号
        /// </summary>
        public string UserNo { set; get; }

        /// <summary>
        /// CAN协议代号
        /// </summary>
        public int CANProtocol { set; get; }

        /// <summary>
        /// RS232协议代号
        /// </summary>
        public int RS232Protocol { set; get; }

        /// <summary>
        /// RS485协议代号
        /// </summary>
        public int RS485Protocol { set; get; }

        /// <summary>
        /// SIM卡的CCID
        /// </summary>
        public string SIMCCID { set; get; }

        /// <summary>
        /// 版本号
        /// </summary>
        public string Version { set; get; }

        /// <summary>
        /// 时间标签
        /// </summary>
        public DateTime Time { set; get; }
    }
}
