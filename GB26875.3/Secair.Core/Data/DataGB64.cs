﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Secair.Core.Data
{
    public class DataGB64
    {
        /// <summary>
        /// 系统类型-名称
        /// </summary>
        public string SystemType{ set; get; }

        /// <summary>
        /// 系统地址
        /// </summary>
        public int SystemAdress { set; get; }

        /// <summary>
        /// 查询操作信息记录数目
        /// </summary>
        public int Count { set; get; }

        /// <summary>
        /// 查询记录的指定起始时问
        /// </summary>
        public DateTime Time { set; get; }
    }
}
