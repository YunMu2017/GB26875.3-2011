﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Secair.Core.Data
{
    public class DataGB3
    {
        /// <summary>
        /// 系统类型-名称
        /// </summary>
        public string SystemType{ set; get; }

        /// <summary>
        /// 系统地址
        /// </summary>
        public int SystemAdress { set; get; }

        /// <summary>
        /// 部件类型
        /// </summary>
        public string UnitType { set; get; }

        /// <summary>
        /// 部件地址
        /// </summary>
        public string UnitAdress { set; get; }

        /// <summary>
        /// 模拟量类型
        /// </summary>
        public string AnalogType { set; get; }

        /// <summary>
        /// 模拟量值
        /// </summary>
        public string AnalogValue { set; get; }

        /// <summary>
        /// 发生时间
        /// </summary>
        public DateTime Time { set; get; }
    }
}
