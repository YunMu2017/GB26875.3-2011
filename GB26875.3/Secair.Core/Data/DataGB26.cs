﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Secair.Core.Data
{
    public class DataGB26
    {
        /// <summary>
        /// 配置说明长度
        /// </summary>
        public int ConfigurationLength { set; get; }

        /// <summary>
        /// 配置说明
        /// </summary>
        public string Configuration { set; get; }
    }
}
