﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Secair.Core.Data
{
    public class Data1
    {
        /// <summary>
        /// 协议说明
        /// </summary>
        private string _msg = "";
        public string msg
        {
            get { return _msg; }
            set { _msg = value; }
        }

        /// <summary>
        /// 回复类型，-1表示无需回复
        /// </summary>
        private int _type = -1;
        public int type
        {
            get { return _type; }
            set { _type = value; }
        }

        /// <summary>
        /// 转发类型，-1表示无需转发
        /// </summary>
        private int _command = -1;
        public int command
        {
            get { return _command; }
            set { _command = value; }
        }

        /// <summary>
        /// 解析日志
        /// </summary>
        private string _msgLog = "";
        public string msgLog
        {
            get { return _msgLog; }
            set { _msgLog = value; }
        }

        /// <summary>
        /// 解析数据
        /// </summary>
        private Object _data = null;
        public Object data
        {
            get { return _data; }
            set { _data = value; }
        }
    }
}
