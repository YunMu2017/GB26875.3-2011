﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Secair.Core.DataStructure;

namespace Secair.Core.Data
{
    public class DataGB24
    {
        /// <summary>
        /// 状态
        /// </summary>
        public DataTDGBOperationInformation Info { set; get; }

        /// <summary>
        /// 操作员编号
        /// </summary>
        public int OpeNo { set; get; }

        /// <summary>
        /// 状态发生时间
        /// </summary>
        public DateTime OpeTime { set; get; }
    }
}
