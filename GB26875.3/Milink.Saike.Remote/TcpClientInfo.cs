﻿using System;

namespace Milink.SaiKe.Remote
{
    public class TcpClientInfo
    {
        public IntPtr connectId;
        public string ip;
        public ushort port;
        public string deviceId;
    }
}
