using System;
using System.Reflection;

// 公司及版权信息
[assembly: AssemblyCompany("广东米量信息科技有限公司")]
[assembly: AssemblyCopyright("Copyright 2018 Milink. All Rights Reserved.")]

// 产品及版本信息
[assembly: AssemblyProduct("Milink-IoT")]
//[assembly: AssemblyInformationalVersion("1.0 Release")]
[assembly: AssemblyTrademark("Milink")]
[assembly: AssemblyCulture("")]

// 程序集版本信息
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyVersion("1.0.*")]
//[assembly: AssemblyFileVersion("1.0.*")]

//[assembly: CLSCompliant(true)]
