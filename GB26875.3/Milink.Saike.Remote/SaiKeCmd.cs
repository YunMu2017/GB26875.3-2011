﻿using System;

namespace Milink.SaiKe.Remote
{
    public class SaiKeCmd
    {
        public string deviceId;
        public byte command;
        public DateTime eventTime;
        public string rawData;
    }
}