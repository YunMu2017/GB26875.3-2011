﻿
namespace Milink.SaiKe.Remote
{
    public class SaiKe151
    {
        public string firmwareId = "";    //固件ID

        public SaiKe151(byte[] data)
        {
            int dataLength = data.Length;
            if (dataLength > 11)
            {
                firmwareId = data[SaiKeUtility.BodyBytePos].ToString().PadLeft(3, '0')
                            + data[SaiKeUtility.BodyBytePos + 1].ToString().PadLeft(3, '0')
                            + data[SaiKeUtility.BodyBytePos + 2].ToString().PadLeft(3, '0')
                            + (data[SaiKeUtility.BodyBytePos + 3] * 256 + data[SaiKeUtility.BodyBytePos + 4]).ToString().PadLeft(5, '0');
            }
        }
    }
} 