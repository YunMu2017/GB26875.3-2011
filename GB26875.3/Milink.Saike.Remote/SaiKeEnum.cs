﻿using Milink.Core;

namespace Milink.SaiKe.Remote
{
    #region 报警类型
    public enum AlarmType
    {
        [EnumDescription("无")]
        Type0 = 0,
        [EnumDescription("火警")]
        Type1 = 1,
        [EnumDescription("监管")]
        Type2,
        [EnumDescription("预警")]
        Type3,
        [EnumDescription("启动")]
        Type4,
        [EnumDescription("反馈")]
        Type5,
        [EnumDescription("停止")]
        Type6,
        [EnumDescription("故障")]
        Type7,
        [EnumDescription("通讯故障")]
        Type8,
        [EnumDescription("")]
        Type9,
        [EnumDescription("传感故障")]
        Type10,
        [EnumDescription("类型错误")]
        Type11,
        [EnumDescription("没有编程")]
        Type12,
        [EnumDescription("屏蔽")]
        Type13,
        [EnumDescription("短路")]
        Type14,
        [EnumDescription("断路")]
        Type15,
        [EnumDescription("延时")]
        Type16,
        [EnumDescription("输入线故障")]
        Type17,
        [EnumDescription("输出线故障")]
        Type18,
        [EnumDescription("24V故障")]
        Type19,
        [EnumDescription("交流故障")]
        Type20,
        [EnumDescription("电池故障")]
        Type21,
        [EnumDescription("总线故障")]
        Type22,
        [EnumDescription("开门")]
        Type23,
        [EnumDescription("关门")]
        Type24,
        [EnumDescription("")]
        Type25,
        [EnumDescription("")]
        Type26,
        [EnumDescription("关门故障")]
        Type27,
        [EnumDescription("开机")]
        Type28,
        [EnumDescription("关机")]
        Type29,
        [EnumDescription("复位")]
        Type30,
        [EnumDescription("自检")]
        Type31,
        [EnumDescription("开门故障")]
        Type32,
        [EnumDescription("欠压")]
        Type33,
        [EnumDescription("过压")]
        Type34,
        [EnumDescription("错相")]
        Type35,
        [EnumDescription("缺相")]
        Type36,
        [EnumDescription("过载")]
        Type37,
        [EnumDescription("供电中断")]
        Type38,
        [EnumDescription("漏电")]
        Type39,
        [EnumDescription("过温")]
        Type40,
        [EnumDescription("复位")]
        Type128 = 128
    }
    #endregion

    #region 部件类型
    public enum ComponentType
    {
        [EnumDescription("未知")]
        Type0 = 0,
        [EnumDescription("感烟")]
        Type1 = 1,
        [EnumDescription("感温")]
        Type2,
        [EnumDescription("手报")]
        Type3,
        [EnumDescription("消火栓")]
        Type4,
        [EnumDescription("输入")]
        Type5,
        [EnumDescription("中继")]
        Type6,
        [EnumDescription("输出")]
        Type7,
        [EnumDescription("输入/出")]
        Type8,
        [EnumDescription("声光")]
        Type9,
        [EnumDescription("显示盘")]
        Type10,
        [EnumDescription("烟温复合")]
        Type11,
        [EnumDescription("双输入")]
        Type12,
        [EnumDescription("双入双出")]
        Type13,
        [EnumDescription("编码中继")]
        Type14,
        [EnumDescription("无线中继")]
        Type15,
        [EnumDescription("可燃气体")]
        Type16,
        [EnumDescription("网卡")]
        Type17,
        [EnumDescription("气体模块")]
        Type18,
        [EnumDescription("多线模块")]
        Type19,
        [EnumDescription("控制器")]
        Type20,
        [EnumDescription("回路板")]
        Type21,
        [EnumDescription("气体盘")]
        Type22,
        [EnumDescription("总线盘")]
        Type23,
        [EnumDescription("多线盘")]
        Type24,
        [EnumDescription("单常闭门")]
        Type25,
        [EnumDescription("双常闭门")]
        Type26,
        [EnumDescription("单常开门")]
        Type27,
        [EnumDescription("双常开门")]
        Type28,
        [EnumDescription("剩余电流")]
        Type29,
        [EnumDescription("四路漏电")]
        Type30,
        [EnumDescription("温度传感")]
        Type31,
        [EnumDescription("三相压流")]
        Type32,
        [EnumDescription("三相电压")]
        Type33,
        [EnumDescription("三相一压")]
        Type34,
        [EnumDescription("单相压流")]
        Type35,
        [EnumDescription("单相电压")]
        Type36,
        [EnumDescription("单相一压")]
        Type37,
        [EnumDescription("三相电流")]
        Type38,
        [EnumDescription("直流电流")]
        Type39,
        [EnumDescription("继电器卡")]
        Type40,
        [EnumDescription("存储卡")]
        Type41
    }
    #endregion

    #region 设备类型
    public enum DeviceType
    {
        //输入设备
        [EnumDescription("输入模块")]
        Type0 = 0,
        [EnumDescription("压力开关")]
        Type1 = 1,
        [EnumDescription("水流指示")]
        Type2,
        [EnumDescription("信号蝶阀")]
        Type3,
        [EnumDescription("火焰探测")]
        Type4,
        [EnumDescription("防火阀入")]
        Type5,
        [EnumDescription("空气采样")]
        Type6,
        [EnumDescription("激光对射")]
        Type7,
        [EnumDescription("感温电缆")]
        Type8,
        [EnumDescription("光纤光栅")]
        Type9,
        [EnumDescription("图形探测")]
        Type10,
        [EnumDescription("接口模块")]
        Type11,
        //输入传感器
        [EnumDescription("中继")]
        Type1000 = 1000,
        [EnumDescription("感烟")]
        Type1001,
        [EnumDescription("感温")]
        Type1002,
        [EnumDescription("手报")]
        Type1003,
        [EnumDescription("火焰探测")]
        Type1004,
        [EnumDescription("空气采样")]
        Type1005,
        [EnumDescription("激光对射")]
        Type1006,
        [EnumDescription("感温电缆")]
        Type1007,
        [EnumDescription("光纤光栅")]
        Type1008,
        [EnumDescription("图形探测")]
        Type1009,
        [EnumDescription("烟温复合")]
        Type1010,
        [EnumDescription("水压传感")]
        Type1011,
        [EnumDescription("液位传感")]
        Type1012,
        //输出设备
        [EnumDescription("消防泵")]
        Type2000 = 2000,
        [EnumDescription("喷淋泵")]
        Type2001,
        [EnumDescription("排烟风阀")]
        Type2002,
        [EnumDescription("正压风阀")]
        Type2003,
        [EnumDescription("")]
        Type2004,
        [EnumDescription("排烟风机")]
        Type2005,
        [EnumDescription("正压风机")]
        Type2006,
        [EnumDescription("挡烟垂壁")]
        Type2007,
        [EnumDescription("卷帘半降")]
        Type2008,
        [EnumDescription("卷帘全降")]
        Type2009,
        [EnumDescription("电梯迫降")]
        Type2010,
        [EnumDescription("消防电源")]
        Type2011,
        [EnumDescription("照明电源")]
        Type2012,
        [EnumDescription("消防警铃")]
        Type2013,
        [EnumDescription("消防广播")]
        Type2014,
        [EnumDescription("应急照明 ")]
        Type2015,
        [EnumDescription("防火门窗")]
        Type2016,
        [EnumDescription("通风空调")]
        Type2017,
        [EnumDescription("防火阀出")]
        Type2018,
        [EnumDescription("疏散指示")]
        Type2019,
        [EnumDescription("防盗输出")]
        Type2020,
        [EnumDescription("消防水幕")]
        Type2021
    }
    #endregion

    #region 应答类型
    public enum AckType
    {
        [EnumDescription("错误")]
        Type0 = 0,
        [EnumDescription("正确")]
        Type1 = 1,
        [EnumDescription("数据发的不合法")]
        Type2
    }
    #endregion

    #region 当前报警类型
    public enum CurrentAlarmType
    {
        [EnumDescription("当前气体启动")]
        Type1 = 1,
        [EnumDescription("当前火警")]
        Type2,
        [EnumDescription("当前气体故障")]
        Type5 = 5,
        [EnumDescription("当前报警故障")]
        Type6,
        [EnumDescription("当前预警信息")]
        Type7,
        [EnumDescription("当前屏蔽信息")]
        Type8,
        [EnumDescription("当前动作")]
        Type10 = 10,
        [EnumDescription("当前监管")]
        Type21 = 21,
        [EnumDescription("当前报警")]
        Type24 = 24,
        [EnumDescription("当前状态信息")]
        Type25,
        [EnumDescription("当前全部报警")]
        Type85 = 85
    }
    #endregion

    #region 历史报警类型
    public enum HistoryAlarmType
    {
        [EnumDescription("启动")]
        Type11 = 11,
        [EnumDescription("隔离")]
        Type12,
        [EnumDescription("动作")]
        Type13,
        [EnumDescription("故障")]
        Type14,
        [EnumDescription("复位")]
        Type15,
        [EnumDescription("开关机")]
        Type16,
        [EnumDescription("按键")]
        Type17,
        [EnumDescription("气体操作")]
        Type18,
        [EnumDescription("监管")]
        Type19,
        [EnumDescription("火警")]
        Type20
    }
    #endregion
}
