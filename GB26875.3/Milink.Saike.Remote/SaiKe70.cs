﻿
namespace Milink.SaiKe.Remote
{
    public class SaiKe70
    {
        public byte host = 0;    //主机号
        public byte loop = 0;    //回路
        public byte start = 0;   //开始地址
        public byte end = 0;     //终止地址
        public string data = "";
    }
}