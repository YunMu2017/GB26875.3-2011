﻿using Milink.Core;
using System.Collections.Generic;

namespace Milink.SaiKe.Remote
{
    public class SaiKe6
    {
        public byte host = 0;                                   //主机号
        public byte loop = 0;                                   //回路
        public List<SaiKe6Data> data = new List<SaiKe6Data>();  //数据

        public SaiKe6()
        {

        }
        public SaiKe6(byte[] buf)
        {
            if (buf.Length > SaiKeUtility.BodyBytePos + 22)
            {
                for (int i = SaiKeUtility.BodyBytePos; i < buf.Length - 22; i++)
                {
                    data.Add(new SaiKe6Data
                    {
                        componentType = buf[i],
                        componentTypeText = EnumDescription.getDescription<ComponentType>(buf[i]),
                        address = buf[++i],
                        message = System.Text.Encoding.GetEncoding("gb2312").GetString(buf, ++i, 20).TrimEnd('\0').Replace('\0', '-')
                    });
                    i += 19;
                }
            }
        }
    }


    public class SaiKe6Data
    {
        public byte componentType = 0;          //部件类型
        public byte address = 0;                //地址
        public string message = "";             //备注信息
        public string componentTypeText = "";   //部件类型-文本
    }
}