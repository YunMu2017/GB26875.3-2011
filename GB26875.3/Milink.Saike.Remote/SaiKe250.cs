﻿
using Milink.Core;

namespace Milink.SaiKe.Remote
{
    public class SaiKe250
    {
        public string ackType = "";     //应答类型
        public string ackCommand = "";  //原始命令
        public string ackTypeText = ""; //应答类型-文本

        public SaiKe250(byte[] data)
        {
            int dataLength = data.Length;
            if (dataLength > 8)
            {
                ackType = data[SaiKeUtility.BodyBytePos].ToString();
                ackCommand = data[SaiKeUtility.BodyBytePos + 1].ToString();
                ackTypeText = EnumDescription.getDescription<AckType>(data[SaiKeUtility.BodyBytePos]);
            }
        }
    }
}