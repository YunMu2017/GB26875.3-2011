﻿
namespace Milink.SaiKe.Remote
{
    public class SaiKe152
    {
        public string firmwareVersion = "";    //固件版本

        public SaiKe152(byte[] data)
        {
            int dataLength = data.Length;
            if (dataLength > 10)
            {
                firmwareVersion = data[SaiKeUtility.BodyBytePos].ToString().PadLeft(3, '0') + "."
                                + data[SaiKeUtility.BodyBytePos + 1].ToString().PadLeft(3, '0') + "."
                                + data[SaiKeUtility.BodyBytePos + 2].ToString().PadLeft(3, '0') + "."
                                + data[SaiKeUtility.BodyBytePos + 3].ToString().PadLeft(3, '0');
            }
        }
    }
}