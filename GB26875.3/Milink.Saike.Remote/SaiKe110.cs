﻿
using Milink.Core;

namespace Milink.SaiKe.Remote
{
    public class SaiKe110
    {
        private const int posAlarmType = SaiKeUtility.BodyBytePos;
        private const int posComponentType = SaiKeUtility.BodyBytePos + 1;
        private const int posLoop = SaiKeUtility.BodyBytePos + 2;
        private const int posDeviceAddress = SaiKeUtility.BodyBytePos + 3;
        private const int posZone = SaiKeUtility.BodyBytePos + 4;

        private const int posHostAddress = SaiKeUtility.BodyBytePos + 6;
        private const int posDeviceType = SaiKeUtility.BodyBytePos + 7;
        private const int posAlarmValue = SaiKeUtility.BodyBytePos + 8;

        private const int posMessage = SaiKeUtility.BodyBytePos + 10;

        private const int posAlarmTime = SaiKeUtility.BodyBytePos + 30;


        public string alarmType = "";           //报警类型
        public string componentType = "";       //部件类型
        public string loop = "";                //回路
        public string deviceAddress = "";       //设备地址
        public string zone = "";                //分区
        public string hostAddress = "";         //控制器地址
        public string deviceType = "";          //设备类型
        public string alarmValue = "";          //报警值
        public string message = "";             //备注信息
        public string alarmTime = "";           //报警时间

        public string alarmTypeText = "";       //报警类型-文本
        public string componentTypeText = "";   //部件类型-文本
        public string deviceTypeText = "";      //设备类型-文本

        public SaiKe110(byte[] data)
        {
            int dataLength = data.Length;
            if (dataLength > posAlarmType)
            {
                alarmType = data[posAlarmType].ToString();
                if (data[posAlarmType] > 128)
                {
                    alarmTypeText = EnumDescription.getDescription<AlarmType>(data[posAlarmType] - 128) + "-解除";
                }
                else
                {
                    alarmTypeText = EnumDescription.getDescription<AlarmType>(data[posAlarmType]);
                }
            }
            if (dataLength > posComponentType)
            {
                componentType = data[posComponentType].ToString();
                componentTypeText = EnumDescription.getDescription<ComponentType>(data[posComponentType]);
            }
            if (dataLength > posLoop)
                loop = data[posLoop].ToString();
            if (dataLength > posDeviceAddress)
                deviceAddress = data[posDeviceAddress].ToString();
            if (dataLength > posZone + 1)
                zone = ByteHelper.twoHexToShort(data, posZone).ToString();
            if (dataLength > posHostAddress)
                hostAddress = data[posHostAddress].ToString();
            if (dataLength > posDeviceType)
            {
                deviceType = data[posDeviceType].ToString();
                switch (deviceType)
                {
                    case "5":
                        deviceTypeText = EnumDescription.getDescription<DeviceType>(data[posDeviceType]);
                        break;
                    case "6":
                        deviceTypeText = EnumDescription.getDescription<DeviceType>(data[posDeviceType] + 1000);
                        break;
                    case "7":
                    case "8":
                    case "19":
                        deviceTypeText = EnumDescription.getDescription<DeviceType>(data[posDeviceType] + 2000);
                        break;
                    default:
                        deviceTypeText = "";
                        break;
                }
            }
            if (dataLength > posAlarmValue + 1)
                alarmValue = ByteHelper.twoHexToShort(data, posAlarmValue).ToString();
            if (dataLength > posMessage + 19)
                message = System.Text.Encoding.GetEncoding("gb2312").GetString(data, posMessage, 20).TrimEnd('\0').Replace('\0', '-');
            if (dataLength > posAlarmTime + 3)
            {
                int tmp = data[posAlarmTime] | (data[posAlarmTime + 1] << 8) | (data[posAlarmTime + 2] << 16) | (data[posAlarmTime + 3] << 24);
                alarmTime = string.Format("20{0}-{1}-{2} {3}:{4}:{5}"
                    , (tmp & 0x3F).ToString("00")
                    , ((tmp & 0x03C0) >> 6).ToString("00")      //32-6-6-5-5-4=6
                    , ((tmp & 0x7C00) >> 10).ToString("00")     //32-6-6-5-5=10
                    , ((tmp & 0x0F8000) >> 15).ToString("00")   //32-6-6-5=15
                    , ((tmp & 0x03F00000) >> 20).ToString("00") //32-6-6=20
                    , ((tmp & 0xFC000000) >> 26).ToString("00") //32-6=26
                    );
            }
        }
    }
}