﻿using Milink.Core;
using System.Collections.Generic;

namespace Milink.SaiKe.Remote
{
    public class SaiKe4
    {
        public byte host = 0;                                   //主机号
        public List<SaiKe4Data> data = new List<SaiKe4Data>();  //数据

        public SaiKe4(byte[] buf)
        {
            if (buf.Length > SaiKeUtility.BodyBytePos + 2)
            {
                for (int i = SaiKeUtility.BodyBytePos; i < buf.Length - 2; i++)
                {
                    data.Add(new SaiKe4Data
                    {
                        componentType = buf[i],
                        componentTypeText = EnumDescription.getDescription<ComponentType>(buf[i]),
                        componentCount = buf[++i]
                    });
                }
            }
        }
    }

    public class SaiKe4Data
    {
        public byte componentType = 0;          //部件类型
        public byte componentCount = 0;         //部件数量
        public string componentTypeText = "";   //部件类型-文本
    }
}