﻿
namespace Milink.SaiKe.Remote
{
    public class SaiKe62
    {
        public byte host = 0;                       //主机号
        public byte historyAlarmType = 0;           //历史报警类型
        public ushort count = 0;                    //报警数量
        public string historyAlarmTypeText = "";    //历史报警类型-文本

        public SaiKe62()
        {
        }
    }
}
