﻿using Milink.Core;
using System.Collections.Generic;

namespace Milink.SaiKe.Remote
{
    public class SaiKe61
    {
        public byte host = 0;               //主机号
        public byte historyAlarmType = 0;   //历史报警类型
        public ushort start = 0;            //开始地址
        public ushort end = 0;              //终止地址
        public List<SaiKe61Data> data = new List<SaiKe61Data>();  //数据

        public SaiKe61()
        {
        }

        public SaiKe61(byte[] buf)
        {
            if (buf.Length > SaiKeUtility.BodyBytePos + 8)
            {
                for (int i = SaiKeUtility.BodyBytePos; i < buf.Length - 8; i++)
                {
                    int tmp = buf[i + 4] | (buf[i + 5] << 8) | (buf[i + 6] << 16) | (buf[i + 7] << 24);
                    data.Add(new SaiKe61Data
                    {
                        alarmType = buf[i],
                        alarmTypeText = buf[i] > 128 ? EnumDescription.getDescription<AlarmType>(buf[i] - 128) + "-解除" : EnumDescription.getDescription<AlarmType>(buf[i]),
                        deviceType = buf[i + 1],
                        deviceTypeText = EnumDescription.getDescription<DeviceType>(buf[i + 1] + 1000),
                        port = (byte)(buf[i + 2] >> 5),
                        loop = (byte)(buf[i + 2] & 0x1F),
                        address = buf[i + 3],
                        alarmTime = string.Format("20{0}-{1}-{2} {3}:{4}:{5}"
                            , (tmp & 0x3F).ToString("00")
                            , ((tmp & 0x03C0) >> 6).ToString("00")      //32-6-6-5-5-4=6
                            , ((tmp & 0x7C00) >> 10).ToString("00")     //32-6-6-5-5=10
                            , ((tmp & 0x0F8000) >> 15).ToString("00")   //32-6-6-5=15
                            , ((tmp & 0x03F00000) >> 20).ToString("00") //32-6-6=20
                            , ((tmp & 0xFC000000) >> 26).ToString("00") //32-6=26
                            )
                    });
                    i += 7;
                }
            }
        }
    }

    public class SaiKe61Data
    {
        public byte alarmType = 0;      //报警类型
        public byte deviceType = 0;     //设备类型
        public byte port = 0;           //部件端口号
        public byte loop = 0;           //回路
        public byte address = 0;        //设备地址
        public string alarmTime = "";   //报警时间

        public string alarmTypeText = "";   //报警类型-文本
        public string deviceTypeText = "";  //设备类型-文本
    }
}
