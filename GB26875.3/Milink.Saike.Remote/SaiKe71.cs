﻿
namespace Milink.SaiKe.Remote
{
    public class SaiKe71
    {
        public byte host = 0;    //主机号
        public byte type = 0;    //类型
        public ushort start = 0;   //开始地址
        public ushort end = 0;     //终止地址
        public string data = "";
    }
}