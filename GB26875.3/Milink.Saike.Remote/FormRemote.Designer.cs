﻿namespace Milink.SaiKe.Remote
{
    partial class FormRemote
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnStopTcp = new System.Windows.Forms.Button();
            this.btnStartTcp = new System.Windows.Forms.Button();
            this.txtHttpPort = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.chkDebug = new System.Windows.Forms.CheckBox();
            this.chkLite = new System.Windows.Forms.CheckBox();
            this.txtAppUrl = new System.Windows.Forms.TextBox();
            this.btnStopHttp = new System.Windows.Forms.Button();
            this.btnStartHttp = new System.Windows.Forms.Button();
            this.txtTcpPort = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lbxMsg = new System.Windows.Forms.ListBox();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.btnStopTcp);
            this.panel1.Controls.Add(this.btnStartTcp);
            this.panel1.Controls.Add(this.txtHttpPort);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.chkDebug);
            this.panel1.Controls.Add(this.chkLite);
            this.panel1.Controls.Add(this.txtAppUrl);
            this.panel1.Controls.Add(this.btnStopHttp);
            this.panel1.Controls.Add(this.btnStartHttp);
            this.panel1.Controls.Add(this.txtTcpPort);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(484, 78);
            this.panel1.TabIndex = 4;
            // 
            // btnStopTcp
            // 
            this.btnStopTcp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStopTcp.Location = new System.Drawing.Point(329, 40);
            this.btnStopTcp.Name = "btnStopTcp";
            this.btnStopTcp.Size = new System.Drawing.Size(68, 26);
            this.btnStopTcp.TabIndex = 26;
            this.btnStopTcp.Text = "StopTcp";
            this.btnStopTcp.UseVisualStyleBackColor = true;
            this.btnStopTcp.Click += new System.EventHandler(this.btnStopTcp_Click);
            // 
            // btnStartTcp
            // 
            this.btnStartTcp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStartTcp.Location = new System.Drawing.Point(329, 7);
            this.btnStartTcp.Name = "btnStartTcp";
            this.btnStartTcp.Size = new System.Drawing.Size(68, 26);
            this.btnStartTcp.TabIndex = 25;
            this.btnStartTcp.Text = "StartTcp";
            this.btnStartTcp.UseVisualStyleBackColor = true;
            this.btnStartTcp.Click += new System.EventHandler(this.btnStartTcp_Click);
            // 
            // txtHttpPort
            // 
            this.txtHttpPort.Location = new System.Drawing.Point(116, 15);
            this.txtHttpPort.Name = "txtHttpPort";
            this.txtHttpPort.Size = new System.Drawing.Size(40, 21);
            this.txtHttpPort.TabIndex = 24;
            this.txtHttpPort.Text = "24444";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(81, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 12);
            this.label2.TabIndex = 23;
            this.label2.Text = "http";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(23, 12);
            this.label1.TabIndex = 22;
            this.label1.Text = "tcp";
            // 
            // chkDebug
            // 
            this.chkDebug.AutoSize = true;
            this.chkDebug.Location = new System.Drawing.Point(216, 17);
            this.chkDebug.Name = "chkDebug";
            this.chkDebug.Size = new System.Drawing.Size(54, 16);
            this.chkDebug.TabIndex = 21;
            this.chkDebug.Text = "Debug";
            this.chkDebug.UseVisualStyleBackColor = true;
            // 
            // chkLite
            // 
            this.chkLite.AutoSize = true;
            this.chkLite.Checked = true;
            this.chkLite.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkLite.Location = new System.Drawing.Point(162, 17);
            this.chkLite.Name = "chkLite";
            this.chkLite.Size = new System.Drawing.Size(48, 16);
            this.chkLite.TabIndex = 20;
            this.chkLite.Text = "Lite";
            this.chkLite.UseVisualStyleBackColor = true;
            // 
            // txtAppUrl
            // 
            this.txtAppUrl.Location = new System.Drawing.Point(6, 44);
            this.txtAppUrl.Name = "txtAppUrl";
            this.txtAppUrl.Size = new System.Drawing.Size(306, 21);
            this.txtAppUrl.TabIndex = 19;
            // 
            // btnStopHttp
            // 
            this.btnStopHttp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStopHttp.Location = new System.Drawing.Point(403, 40);
            this.btnStopHttp.Name = "btnStopHttp";
            this.btnStopHttp.Size = new System.Drawing.Size(68, 26);
            this.btnStopHttp.TabIndex = 18;
            this.btnStopHttp.Text = "StopHttp";
            this.btnStopHttp.UseVisualStyleBackColor = true;
            this.btnStopHttp.Click += new System.EventHandler(this.btnStopHttp_Click);
            // 
            // btnStartHttp
            // 
            this.btnStartHttp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStartHttp.Location = new System.Drawing.Point(403, 7);
            this.btnStartHttp.Name = "btnStartHttp";
            this.btnStartHttp.Size = new System.Drawing.Size(68, 26);
            this.btnStartHttp.TabIndex = 17;
            this.btnStartHttp.Text = "StartHttp";
            this.btnStartHttp.UseVisualStyleBackColor = true;
            this.btnStartHttp.Click += new System.EventHandler(this.btnStartHttp_Click);
            // 
            // txtTcpPort
            // 
            this.txtTcpPort.Location = new System.Drawing.Point(35, 15);
            this.txtTcpPort.Name = "txtTcpPort";
            this.txtTcpPort.Size = new System.Drawing.Size(40, 21);
            this.txtTcpPort.TabIndex = 16;
            this.txtTcpPort.Text = "14444";
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.lbxMsg);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 78);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(484, 384);
            this.panel2.TabIndex = 5;
            // 
            // lbxMsg
            // 
            this.lbxMsg.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lbxMsg.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbxMsg.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.lbxMsg.FormattingEnabled = true;
            this.lbxMsg.HorizontalScrollbar = true;
            this.lbxMsg.ItemHeight = 12;
            this.lbxMsg.Location = new System.Drawing.Point(0, 0);
            this.lbxMsg.Name = "lbxMsg";
            this.lbxMsg.Size = new System.Drawing.Size(482, 382);
            this.lbxMsg.TabIndex = 20;
            this.lbxMsg.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.lbxMsg_DrawItem);
            // 
            // FormRemote
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(484, 462);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.Name = "FormRemote";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "模拟-控制器";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormRemote_FormClosing);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox chkDebug;
        private System.Windows.Forms.CheckBox chkLite;
        private System.Windows.Forms.TextBox txtAppUrl;
        private System.Windows.Forms.Button btnStopHttp;
        private System.Windows.Forms.Button btnStartHttp;
        private System.Windows.Forms.TextBox txtTcpPort;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ListBox lbxMsg;
        private System.Windows.Forms.TextBox txtHttpPort;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnStopTcp;
        private System.Windows.Forms.Button btnStartTcp;
    }
}

